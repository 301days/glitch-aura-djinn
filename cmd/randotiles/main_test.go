package main

import (
	"encoding/xml"
	"strings"
	"testing"

	"gitlab.com/301days/glitch-aura-djinn/pkg/world"
)

// Note that we're running the same set of tests through four different functions.

type args struct {
	mapHeader   world.MapHeader
	mapContents world.MapContents
}
type test struct {
	name string
	args args
	want string
}

var tests = []test{
	{
		name: "One cell at origin",
		args: args{
			mapHeader: world.MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZName: "onecell", Outdoor: false,
				NoRecall: false, AlwaysDark: false, TownLimits: false},
			mapContents: world.MapContents{
				Cells: map[world.CellKey]world.Cell{
					{X: 0, Y: 0, Z: 0}: {Graphic: "[]"},
				},
			},
		},
		want: strings.Join([]string{`<z>0</z> <x>0</x> <y>0</y> <n>onecell</n>`, `[]`}, "\n") + "\n",
	},
	{
		name: "One cell at origin with key values 1",
		args: args{
			mapHeader: world.MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZName: "onecell", Outdoor: true,
				NoRecall: true, AlwaysDark: false, TownLimits: false},
			mapContents: world.MapContents{
				Cells: map[world.CellKey]world.Cell{
					{X: 0, Y: 0, Z: 0}: {"[]"},
				},
			},
		},
		want: strings.Join([]string{
			`<z>0</z> <x>0</x> <y>0</y> <n>onecell</n> norecall=true outdoor=true`,
			`[]`}, "\n") + "\n",
	},
	{
		name: "One cell at origin with key values 2",
		args: args{
			mapHeader: world.MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZName: "onecell", Outdoor: false,
				NoRecall: false, AlwaysDark: true, TownLimits: true},
			mapContents: world.MapContents{
				Cells: map[world.CellKey]world.Cell{
					{X: 0, Y: 0, Z: 0}: {Graphic: "[]"},
				},
			},
		},
		want: strings.Join([]string{
			`<z>0</z> <x>0</x> <y>0</y> <n>onecell</n> alwaysdark=true townlimits=true`,
			`[]`}, "\n") + "\n",
	},
	{
		name: "One cell at origin with comments",
		args: args{
			mapHeader: world.MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZName: "onecell", Outdoor: false,
				NoRecall: false, AlwaysDark: false, TownLimits: false, Comments: []string{"comment1", "comment2"}},
			mapContents: world.MapContents{
				Cells: map[world.CellKey]world.Cell{
					{0, 0, 0}: {"[]"},
				},
			},
		},
		want: strings.Join([]string{`//comment1`, `//comment2`, `<z>0</z> <x>0</x> <y>0</y> <n>onecell</n>`, `[]`}, "\n") + "\n",
	},
	{
		name: "Multiple lines of cells at origin",
		args: args{
			mapHeader: world.MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZName: "multilinescells", Outdoor: false,
				NoRecall: false, AlwaysDark: false, TownLimits: false}, mapContents: world.MapContents{
				Cells: map[world.CellKey]world.Cell{
					{0, 0, 0}: {"[]"},
					{1, 0, 0}: {"[]"},
					{2, 0, 0}: {"[]"},
					{3, 0, 0}: {"[]"},
					{0, 3, 0}: {"[]"},
					{1, 3, 0}: {"[]"},
					{2, 3, 0}: {"[]"},
					{3, 3, 0}: {"[]"},
					{0, 1, 0}: {"[]"},
					{1, 1, 0}: {". "},
					{2, 1, 0}: {". "},
					{3, 1, 0}: {"[]"},
					{0, 2, 0}: {"[]"},
					{1, 2, 0}: {". "},
					{2, 2, 0}: {". "},
					{3, 2, 0}: {"[]"},
				},
			},
		},
		want: strings.Join([]string{
			`<z>0</z> <x>0</x> <y>0</y> <n>multilinescells</n>`,
			`[][][][]`,
			`[]. . []`,
			`[]. . []`,
			`[][][][]`}, "\n") + "\n",
	},
	{
		name: "Multiple lines of cells with offsets and forestry",
		args: args{
			mapHeader: world.MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZName: "multilinescells", Outdoor: false,
				NoRecall: false, AlwaysDark: false, TownLimits: false, ForestryLevel: "light",
				XOffset: 3, YOffset: 4, ZCoord: 5}, mapContents: world.MapContents{
				Cells: map[world.CellKey]world.Cell{
					{3, 4, 5}: {"[]"},
					{4, 4, 5}: {"[]"},
					{5, 4, 5}: {"[]"},
					{6, 4, 5}: {"[]"},
					{3, 7, 5}: {"[]"},
					{4, 7, 5}: {"[]"},
					{5, 7, 5}: {"[]"},
					{6, 7, 5}: {"[]"},
					{3, 5, 5}: {"[]"},
					{4, 5, 5}: {"{}"},
					{5, 5, 5}: {"{}"},
					{6, 5, 5}: {"[]"},
					{3, 6, 5}: {"[]"},
					{4, 6, 5}: {"{}"},
					{5, 6, 5}: {"{}"},
					{6, 6, 5}: {"[]"},
				},
			},
		},
		want: strings.Join([]string{
			`<z>5</z> <x>3</x> <y>4</y> <f>light</f> <n>multilinescells</n>`,
			`[][][][]`,
			`[]{}{}[]`,
			`[]{}{}[]`,
			`[][][][]`}, "\n") + "\n",
	},
}

func Test_randotiles0(t *testing.T) {
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := randotiles0(tt.args.mapHeader, tt.args.mapContents); got != tt.want {
				t.Errorf("randotiles0() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_randotiles1(t *testing.T) {
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := randotiles1(tt.args.mapHeader, tt.args.mapContents); len(got) != len(tt.want) {
				t.Errorf("randotiles1() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_randotiles2(t *testing.T) {
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := randotiles2(tt.args.mapHeader, tt.args.mapContents); len(got) != len(tt.want) {
				t.Errorf("randotiles2() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_randotiles3(t *testing.T) {
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := randotiles3(tt.args.mapHeader, tt.args.mapContents); len(got) != len(tt.want) {
				t.Errorf("randotiles3() = %v, want %v", got, tt.want)
			}
		})
	}
}
