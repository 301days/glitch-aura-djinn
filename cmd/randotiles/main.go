package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"

	"gitlab.com/301days/glitch-aura-djinn/pkg/world"
)

func main() {
	inputFilePath := os.Args[1]
	inputLevelNumber, _ := strconv.Atoi(os.Args[2])
	header, contents, err := world.MapLevelFromFile(inputFilePath, inputLevelNumber)
	if err != nil {
		panic(err)
	}

	fmt.Print(randotiles0(header, contents))
	fmt.Print(randotiles1(header, contents))
	fmt.Print(randotiles2(header, contents))
	fmt.Print(randotiles3(header, contents))
}

// randotiles0 generates a copy of a map with the exact same tiles.
//
// It takes two parameters:
// - header: a MapHeader struct representing the header of the map.
// - contents: a MapContents struct representing the contents of the map.
//
// It returns a string representing the map.
func randotiles0(header world.MapHeader, contents world.MapContents) string {
	newMap := world.MapContents{}
	newMap.Cells = make(map[world.CellKey]world.Cell, len(contents.Cells))
	for key, cell := range contents.Cells {
		newMap.Cells[key] = cell
	}
	return world.MapToString(header, newMap)
}

// randotiles1 generates a copy of a map with the tiles chosen at random from the
// tiles that existed in the original map. No other rules are applied.
//
// It takes two parameters:
// - header: a MapHeader struct representing the header of the map.
// - contents: a MapContents struct representing the contents of the map.
//
// It returns a string representing the map.
func randotiles1(header world.MapHeader, contents world.MapContents) string {
	cellTypes := collectCellTypes(contents.Cells)

	newMap := world.MapContents{
		Cells: make(map[world.CellKey]world.Cell, len(contents.Cells)),
	}

	for key := range contents.Cells {
		newMap.Cells[key] = world.Cell{
			Graphic: cellTypes[rand.Intn(len(cellTypes))],
		}
	}

	return world.MapToString(header, newMap)
}

// randotiles2 generates a copy of a map with the tiles chosen at random from the
// tiles that existed in the original map. The tile choice is weighted by the
// frequency of the tiles in the original map.
//
// It takes two parameters:
// - header: a MapHeader struct representing the header of the map.
// - contents: a MapContents struct representing the contents of the map.
//
// It returns a string representing the map.
func randotiles2(header world.MapHeader, contents world.MapContents) string {
	cellTypes := countCellTypes(contents.Cells)

	newMap := world.MapContents{
		Cells: make(map[world.CellKey]world.Cell, len(contents.Cells)),
	}
	for key := range contents.Cells {
		newMap.Cells[key] = world.Cell{Graphic: weightedRandomString(cellTypes)}
	}
	result := world.MapToString(header, newMap)
	return result
}

// randotiles3 generates a copy of a map with the tiles chosen at random from
// the tiles that existed in the original map. The tile choice is weighted by
// the frequency of the tiles in the original map, with enforcement of the
// frequency of the tiles in the map. For any given tile, the same number of
// instances will occur in the original and the copy.
//
// It takes two parameters:
// - header: a MapHeader struct representing the header of the map.
// - contents: a MapContents struct representing the contents of the map.
//
// It returns a string representing the map.
func randotiles3(header world.MapHeader, contents world.MapContents) string {
	cellTypes := countCellTypes(contents.Cells)

	newMap := world.MapContents{
		Cells: make(map[world.CellKey]world.Cell, len(contents.Cells)),
	}
	for key := range contents.Cells {
		graphic := weightedRandomString(cellTypes)
		newMap.Cells[key] = world.Cell{Graphic: graphic}
		cellTypes[graphic]--
	}
	result := world.MapToString(header, newMap)
	return result
}

// collectCellTypes returns a string slice containing the unique cell types in the given map.
//
// cells: a map where the key is of type world.CellKey and the value is of type world.Cell.
//
// returns: a string slice containing the unique cell types.
func collectCellTypes(cells map[world.CellKey]world.Cell) []string {
	cellTypes := make(map[string]bool)

	for _, cell := range cells {
		cellTypes[cell.Graphic] = true
	}

	keys := make([]string, len(cellTypes))
	i := 0
	for key := range cellTypes {
		keys[i] = key
		i++
	}

	return keys
}

// countCellTypes counts the number of occurrences of each cell type in the given map of cells.
//
// cells: a map of world.CellKey to world.Cell representing the cells to count.
//
// returns: a map of string to int representing the count of each cell type.
func countCellTypes(cells map[world.CellKey]world.Cell) map[string]int {
	cellTypes := make(map[string]int)
	for _, cell := range cells {
		cellTypes[cell.Graphic]++
	}
	return cellTypes
}

// weightedRandomString returns a randomly-chosen string based on the given
// weights.
//
// weights: a map[string]int where the keys are the strings and the values are
// the corresponding weights.
//
// returns: a string which is the randomly picked from the provided map based
// on the weights.
func weightedRandomString(weights map[string]int) string {
	totalWeight := 0
	for _, weight := range weights {
		totalWeight += weight
	}

	guess := rand.Intn(totalWeight)
	for key, weight := range weights {
		guess -= weight
		if guess < 0 {
			return key
		}
	}

	panic("Weights changed during random pick.")
}
