package main

import (
	"bytes"
	"io"
	"os"
	"strings"
	"sync"
	"testing"
)

func Test_main(t *testing.T) {
	// Save the original os.Args
	originalArgs := os.Args

	// Restore the original os.Args after the test
	defer func() { os.Args = originalArgs }()

	tests := []struct {
		name       string
		args       []string
		wantPrefix string
	}{
		{
			name:       "simple test",
			args:       []string{`./main`, `../../testdata/maps/Annwn.txt`},
			wantPrefix: "//Doorway into Ydmos' tower is 1 way entrance",
		},
		{
			name:       "fail test",
			args:       []string{`./main`, `../../testdata/maps/DoesNotExist.txt`},
			wantPrefix: "File not found!",
		},
	}
	for _, tt := range tests {

		t.Run(tt.name, func(t *testing.T) {
			// Set custom arguments for testing
			os.Args = tt.args

			// Create a pipe to redirect stdout
			r, w, _ := os.Pipe()
			originalStdout := os.Stdout

			// Redirect stdout to the write end of the pipe
			os.Stdout = w

			// Restore the original stdout after the test
			defer func() {
				w.Close()
				os.Stdout = originalStdout
			}()

			// Create a buffer to capture the output
			var buf bytes.Buffer

			// Use a sync.WaitGroup to wait for the goroutine to finish reading
			var wg sync.WaitGroup
			wg.Add(1)

			// Start a goroutine to read from the pipe and write to the buffer
			go func() {
				defer wg.Done()
				_, _ = io.Copy(&buf, r)
			}()

			// Call the function that produces the output
			main()

			// Close the write end of the pipe
			w.Close()

			// Wait for the goroutine to finish reading
			wg.Wait()

			// Get the captured output from the buffer
			capturedOutput := buf.String()

			if !strings.HasPrefix(capturedOutput, tt.wantPrefix) {
				t.Errorf("main() = %v, want %v", capturedOutput, tt.wantPrefix)
			}
		})
	}
}
