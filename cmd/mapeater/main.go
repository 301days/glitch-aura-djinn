package main

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/301days/glitch-aura-djinn/pkg/world"
)

func main() {
	inputFilePath := os.Args[1]

	result := ""
	buffer, err := world.ReadFileToString(inputFilePath)
	if err != nil {
		result = "File not found!\n"
	} else {

		// Convert CRs to newlines
		buffer = strings.ReplaceAll(buffer, "\r", "\n")

		result = world.ReadAndWriteMap(buffer)
	}
	fmt.Print(result)
}
