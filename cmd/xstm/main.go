package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"path"
	"runtime"
	"runtime/pprof"
	"strings"
	"time"

	"github.com/muesli/termenv"
	"gitlab.com/301days/glitch-aura-djinn/pkg/wfcish"
	"gitlab.com/301days/glitch-aura-djinn/pkg/world"
)

var (
	cliMapname          = flag.String("mapname", "", "name of source map")
	cliMaplevel         = flag.Int("maplevel", 0, "which source map level to read")
	cliMethod           = flag.String("method", "xstm4", "which method to use")
	cliNeighborDistance = flag.Int("neighbor-distance", 1, "how far to consider neighbors")
	cliMaxRetries       = flag.Int("max-retries", 1, "max times to retry a collapse")
	cliFPC              = flag.Uint("frames-per-collapse", 0, "frames per collapse")
	cliTPF              = flag.Duration("time-per-frame", time.Millisecond, "time per frame in ms")
	cliStyled           = flag.Bool("styled", false, "use styled output")
	cliSeed             = flag.Int64("seed", 0, "random seed to use")
	cliMaxSeed          = flag.Int64("max-seed", 0, "maximum random seed to use (when -match-original is set)")
	cliSeekOriginal     = flag.Bool("seek-original", false, "increment seed until output matches original map")
	cliSeekValid        = flag.Bool("seek-valid", false, "increment seed until output is valid")
	cpuprofile          = flag.String("cpuprofile", "", "write cpu profile to `file`")
	memprofile          = flag.String("memprofile", "", "write memory profile to `file`")
)

func main() {
	var err error
	flag.Parse()

	// CPU profiling for optimization, boilerplate.
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal("could not create CPU profile: ", err)
		}
		defer f.Close() // error handling omitted for example
		if err := pprof.StartCPUProfile(f); err != nil {
			log.Fatal("could not start CPU profile: ", err)
		}
		defer pprof.StopCPUProfile()
	}

	displayCfg := wfcish.DisplayCfg{
		Output:            termenv.NewOutput(os.Stdout),
		FramesPerCollapse: *cliFPC,
		TimePerFrame:      *cliTPF,
		Styled:            *cliStyled,
	}
	retryCfg := wfcish.RetryCfg{
		InitialSeed:       *cliSeed,
		MaxSeed:           *cliMaxSeed,
		MaxRetriesPerSeed: *cliMaxRetries,
		SeekOriginal:      *cliSeekOriginal,
		SeekValid:         *cliSeekValid,
	}
	displayCfg.Output.ClearScreen()
	outputFilePath, finalMap := doIt(*cliMapname, *cliMaplevel, *cliMethod, *cliNeighborDistance, retryCfg, displayCfg)

	fmt.Println(finalMap)
	if !strings.Contains(finalMap, "XX") {
		err = WriteStringToFile(outputFilePath, finalMap)
		if err != nil {
			panic(err)
		}
	} else {
		fmt.Println("FAILED to collapse map.")
	}

	if *memprofile != "" {
		f, err := os.Create(*memprofile)
		if err != nil {
			log.Fatal("could not create memory profile: ", err)
		}
		defer f.Close() // error handling omitted for example
		runtime.GC()    // get up-to-date statistics
		if err := pprof.WriteHeapProfile(f); err != nil {
			log.Fatal("could not write memory profile: ", err)
		}
	}
}

func doIt(mapname string, maplevel int, method string, distance int, retryCfg wfcish.RetryCfg, displayCfg wfcish.DisplayCfg) (string, string) {
	// Read in the level specified on the command line.
	inputFilePath := path.Join("testdata", "maps", mapname+".txt")
	outputFilePath := fmt.Sprint(mapname, "_", maplevel, "_", method, ".out")
	header, contents, err := world.MapLevelFromFile(inputFilePath, maplevel)
	if err != nil {
		panic(err)
	}
	origMap := world.MapToString(header, contents)

	var finalMap string

	fmt.Println("DisplayCfg:", displayCfg)
	fmt.Println("RetryCfg:", retryCfg)

	switch method {
	case "xstm1":
		finalMap = wfcish.XSTM(header, contents,
			wfcish.GatherTiles,
			wfcish.CreateWaveMap,
			false,
			wfcish.DeriveRuleSetExtended,
			distance,
			false,
			func(waveMap *wfcish.WaveMapContents, rules wfcish.RuleMap, distance int) {},
			wfcish.CollapseCell,
			func(waveMap *wfcish.WaveMapContents, collapsedCell world.CellKey) error { return nil },
			retryCfg,
			displayCfg,
			origMap)
	case "xstm2":
		finalMap = wfcish.XSTM(header, contents,
			wfcish.GatherTilesIgnoreNull,
			wfcish.CreateWaveMap,
			true,
			wfcish.DeriveRuleSetExtended,
			distance,
			true,
			wfcish.SetEdgesExtended,
			wfcish.CollapseCell,
			func(waveMap *wfcish.WaveMapContents, collapsedCell world.CellKey) error { return nil },
			retryCfg,
			displayCfg,
			origMap)
	case "xstm3":
		finalMap = wfcish.XSTM(header, contents,
			wfcish.GatherTileWeights,
			wfcish.CreateWaveMap,
			true,
			wfcish.DeriveRuleSetExtended,
			distance,
			true,
			wfcish.SetEdgesExtended,
			wfcish.CollapseCellWeighted,
			func(waveMap *wfcish.WaveMapContents, collapsedCell world.CellKey) error { return nil },
			retryCfg,
			displayCfg,
			origMap)
	case "xstm4":
		finalMap = wfcish.XSTM(header, contents,
			wfcish.GatherTileWeights,
			wfcish.CreateWaveMap,
			true,
			wfcish.DeriveRuleSetExtended,
			distance,
			true,
			wfcish.SetEdgesExtended,
			wfcish.CollapseCellWeighted,
			wfcish.DecreaseWeights,
			retryCfg,
			displayCfg,
			origMap)
	default:
		panic(errors.New("Unknown method: " + method))
	}

	return outputFilePath, finalMap
}

func WriteStringToFile(filename string, output string) error {
	err := os.WriteFile(filename, []byte(output), 0o644)
	return err
}
