package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
	"os/exec"
	"path"
	"runtime"
	"runtime/pprof"
	"strings"
	"time"

	"github.com/muesli/termenv"
	"gitlab.com/301days/glitch-aura-djinn/pkg/wfcish"
	"gitlab.com/301days/glitch-aura-djinn/pkg/world"
)

var cliMapname = flag.String("mapname", "", "name of source map")
var cliMaplevel = flag.Int("maplevel", 0, "which source map level to read")
var cliMethod = flag.String("method", "rstm4", "which method to use")
var cliMaxRetries = flag.Int("max-retries", 1, "max times to retry a collapse")
var cliFPC = flag.Uint("frames-per-collapse", 0, "frames per collapse")
var cliTPF = flag.Duration("time-per-frame", time.Millisecond, "time per frame in ms")
var cliStyled = flag.Bool("styled", false, "use styled output")
var cliSeed = flag.Int64("seed", 0, "random seed to use")
var cliMaxSeed = flag.Int64("max-seed", 0, "maximum random seed to use (when -match-original is set)")
var cliSeekOriginal = flag.Bool("seek-original", false, "increment seed until output matches original map")
var cliSeekValid = flag.Bool("seek-valid", false, "increment seed until output is valid")
var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to `file`")
var memprofile = flag.String("memprofile", "", "write memory profile to `file`")

func main() {
	var err error
	flag.Parse()

	// CPU profiling for optimization, boilerplate.
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal("could not create CPU profile: ", err)
		}
		defer f.Close() // error handling omitted for example
		if err := pprof.StartCPUProfile(f); err != nil {
			log.Fatal("could not start CPU profile: ", err)
		}
		defer pprof.StopCPUProfile()
	}

	displayCfg := wfcish.DisplayCfg{
		Output:            termenv.NewOutput(os.Stdout),
		FramesPerCollapse: *cliFPC,
		TimePerFrame:      *cliTPF,
		Styled:            *cliStyled,
	}
	retryCfg := wfcish.RetryCfg{
		InitialSeed:       *cliSeed,
		MaxSeed:           *cliMaxSeed,
		MaxRetriesPerSeed: *cliMaxRetries,
		SeekOriginal:      *cliSeekOriginal,
		SeekValid:         *cliSeekValid,
	}
	clearScreen()
	outputFilePath, finalMap := doIt(*cliMapname, *cliMaplevel, *cliMethod, retryCfg, displayCfg)

	fmt.Println(finalMap)
	if !strings.Contains(finalMap, "XX") {
		err = WriteStringToFile(outputFilePath, finalMap)
		if err != nil {
			panic(err)
		}
	} else {
		fmt.Println("FAILED to collapse map.")
	}

	if *memprofile != "" {
		f, err := os.Create(*memprofile)
		if err != nil {
			log.Fatal("could not create memory profile: ", err)
		}
		defer f.Close() // error handling omitted for example
		runtime.GC()    // get up-to-date statistics
		if err := pprof.WriteHeapProfile(f); err != nil {
			log.Fatal("could not write memory profile: ", err)
		}
	}
}

func doIt(mapname string, maplevel int, method string, retryCfg wfcish.RetryCfg, displayCfg wfcish.DisplayCfg) (string, string) {

	// Read in the level specified on the command line.
	inputFilePath := path.Join("testdata", "maps", mapname+".txt")
	outputFilePath := fmt.Sprint(mapname, "_", maplevel, "_", method, ".out")
	header, contents, err := world.MapLevelFromFile(inputFilePath, maplevel)
	if err != nil {
		panic(err)
	}
	origMap := world.MapToString(header, contents)

	// set up for the main loop
	var finalMap string
	var done bool
	seed := retryCfg.InitialSeed
	// If either "seek" flag is set, keep calling the method until:
	// 1) if seek-original, the output is the same as the original map,
	// 2) if seek-valid, the collapse completes with a valid map,
	// 3) if max-seed is set, the maximum seed is reached.
	var lastMinute, currMinute int
	lastMinute = time.Now().Minute()
	for !done {
		done = true
		rng := rand.New(rand.NewSource(seed))
		currMinute = time.Now().Minute()
		if displayCfg.FramesPerCollapse > 0 || currMinute != lastMinute {
			displayCfg.Output.WriteString(fmt.Sprint("Processing seed: ", seed, "\n"))
			lastMinute = currMinute
		}

		switch method {
		case "rstm1":
			finalMap = wfcish.RSTM(header, contents,
				wfcish.GatherTiles,
				wfcish.CreateWaveMap,
				false,
				wfcish.DeriveRuleSet,
				func(waveMap *wfcish.WaveMapContents, rules wfcish.RuleMap) {},
				wfcish.CollapseCell,
				func(waveMap *wfcish.WaveMapContents, collapsedCell world.CellKey) error { return nil },
				retryCfg,
				displayCfg,
				rng)
		case "rstm2":
			finalMap = wfcish.RSTM(header, contents,
				wfcish.GatherTilesIgnoreNull,
				wfcish.CreateWaveMap,
				true,
				wfcish.DeriveRuleSetIgnoreNull,
				wfcish.SetEdges,
				wfcish.CollapseCell,
				func(waveMap *wfcish.WaveMapContents, collapsedCell world.CellKey) error { return nil },
				retryCfg,
				displayCfg,
				rng)
		case "rstm3":
			finalMap = wfcish.RSTM(header, contents,
				wfcish.GatherTileWeights,
				wfcish.CreateWaveMap,
				true,
				wfcish.DeriveRuleSetIgnoreNull,
				wfcish.SetEdges,
				wfcish.CollapseCellWeighted,
				func(waveMap *wfcish.WaveMapContents, collapsedCell world.CellKey) error { return nil },
				retryCfg, displayCfg,
				rng)
		case "rstm4":
			finalMap = wfcish.RSTM(header, contents,
				wfcish.GatherTileWeights,
				wfcish.CreateWaveMap,
				true,
				wfcish.DeriveRuleSetIgnoreNull,
				wfcish.SetEdges,
				wfcish.CollapseCellWeighted,
				wfcish.DecreaseWeights,
				retryCfg, displayCfg,
				rng)
		default:
			panic(errors.New("Unknown method: " + method))
		}

		if *cliSeekValid && strings.Contains(finalMap, "XX") {
			done = false
		}
		if *cliSeekOriginal && finalMap != origMap {
			done = false
		}
		seed++
		if *cliMaxSeed != 0 && seed > *cliMaxSeed {
			done = true
		}
	}
	displayCfg.Output.WriteString(fmt.Sprint("Seed: ", seed, "\n"))
	return outputFilePath, finalMap
}

// clearScreen clears the terminal screen.
func clearScreen() {
	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	err := cmd.Run()
	if err != nil {
		fmt.Println(err)
	}
}

// ReadFileToString reads the content of a file and returns it as a string.
//
// It takes a `filename` parameter which is the path to the file to be read.
// It returns a `string` which is the content of the file, and an `error` if any error occurs.
func WriteStringToFile(filename string, output string) error {
	err := os.WriteFile(filename, []byte(output), 0644)
	return err
}
