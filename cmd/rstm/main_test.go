package main

import (
	"fmt"
	"io"
	"os"
	"testing"
	"time"

	"github.com/kylelemons/godebug/diff"
	"github.com/muesli/termenv"
	"gitlab.com/301days/glitch-aura-djinn/pkg/wfcish"
)

var displayCfg = wfcish.DisplayCfg{
	Output:            termenv.NewOutput(io.Discard),
	FramesPerCollapse: 0,
	TimePerFrame:      time.Millisecond * 0,
}

func Test_doIt(t *testing.T) {
	err := os.Chdir("../..") // apparently necessary
	if err != nil {
		t.Fatal(err)
	}

	tests := []struct {
		name       string
		mapname    string
		maplevel   int
		method     string
		displayCfg wfcish.DisplayCfg
		want       string
		want1      string
	}{
		{"Annwn2 rstm1", "Annwn", 2, "rstm1", displayCfg, "Annwn_2_rstm1.out",
			`<z>-25</z> <x>38</x> <y>18</y> <n>Ruined Tavern Cellar</n>
up[_. []. [_[_up[_. up[_dn
up/\/\/\/\/\/\/\/\[]/\[]up
[]/\[]/\/\/\[]/\[]/\/\[]| ` + `
[]/\/\/\[]/\/\[][][][][]| ` + `
| /\/\[]/\[]/\/\[][][][]/\
[]/\[]/\[]/\/\/\[][]/\[]/\
dn/\/\/\/\/\/\[][]/\[][]. ` + `
up[]/\/\/\[]/\/\/\/\[][]. ` + `
/\/\. up/\/\up| up. /\[_[]
`,
		},
		{"Annwn2 rstm2", "Annwn", 2, "rstm2", displayCfg, "Annwn_2_rstm2.out",
			`<z>-25</z> <x>38</x> <y>18</y> <n>Ruined Tavern Cellar</n>
/\/\/\/\/\/\/\/\/\/\/\/\/\
/\/\[][]/\[][][]/\[]/\[]/\
/\/\[]/\[]/\/\/\/\[][]/\/\
/\/\[]/\/\[][][]/\/\[][]/\
/\/\/\/\[]/\[][]/\/\[]/\/\
/\[][]/\/\[][]/\[][][][]/\
/\/\[]/\/\/\[]/\[]/\[]/\/\
/\[]/\/\[]/\[]/\/\/\/\[]/\
/\/\/\/\/\/\/\/\/\/\/\/\/\
`,
		},
		{"Annwn2 rstm3", "Annwn", 2, "rstm3", displayCfg, "Annwn_2_rstm3.out",
			`<z>-25</z> <x>38</x> <y>18</y> <n>Ruined Tavern Cellar</n>
/\/\/\/\/\/\/\/\/\/\/\/\/\
/\/\/\[][][]/\[][]/\[]/\/\
/\/\/\[][][][]/\/\[][][]/\
/\/\/\[]/\[]/\[]/\/\[]/\/\
/\[][]/\[]/\[]/\/\[][]/\/\
/\[]/\[]/\/\/\/\[]/\[]/\/\
/\/\[]/\[]/\[][][]/\[]/\/\
/\/\/\/\[]/\[]/\[]/\[]/\/\
/\/\/\/\/\/\/\/\/\/\/\/\/\
`,
		},
		{"Annwn2 rstm4", "Annwn", 2, "rstm4", displayCfg, "Annwn_2_rstm4.out",
			`<z>-25</z> <x>38</x> <y>18</y> <n>Ruined Tavern Cellar</n>
/\/\/\/\/\/\/\/\/\/\/\/\/\
/\[][][][][][][][][][][]/\
/\[][][][]?!?!XX. . . []/\
/\[][][][]?!. . . . . []/\
/\[][]. . . . . []. [][]/\
/\[]. [][]. . . []. [][]/\
/\[][]. ?!. . . . . . []/\
/\[][][][][][][][][][][]/\
/\/\/\/\/\/\/\/\/\/\/\/\/\
`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			got, got1 := doIt(tt.mapname, tt.maplevel, tt.method, wfcish.RetryCfg{MaxRetriesPerSeed: 1}, tt.displayCfg)
			if got != tt.want {
				t.Errorf("doIt() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				fmt.Println(diff.Diff(got1, tt.want1))
				t.Errorf("doIt() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
