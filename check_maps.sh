#!/bin/bash

for file in ./testdata/maps/*.txt; do
    # Run the Go program on each map file
    go run cmd/mapeater/main.go "$file" > "$file.out"

    # Compare the result to the original file
    diff --strip-trailing-cr "$file" "$file.out"

    # Remove the .out file
    rm "$file.out"
done
