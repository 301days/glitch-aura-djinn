package world

import (
	"bytes"
	"encoding/xml"
	"errors"
	"fmt"
	"os"
	"reflect"
	"regexp"
	"strings"
	"testing"
)

func Test_processXMLHeader(t *testing.T) {
	type args struct {
		mapline string
	}
	tests := []struct {
		name    string
		args    args
		want    MapHeader
		wantErr error
	}{
		{
			name: "minimal",
			args: args{
				mapline: `<mapheader> <z>15</z> <x>10</x> <y>5</y> <n>zname</n> </mapheader>`,
			},
			want: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZCoord: 15, XOffset: 10, YOffset: 5,
				ZName: "zname"},
		},
		{
			name: "forestry",
			args: args{
				mapline: `<mapheader> <z>15</z> <x>10</x> <y>5</y> <f>heavy</f> <n>zname</n> </mapheader>`,
			},
			want: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZCoord: 15, XOffset: 10, YOffset: 5,
				ZName: "zname", ForestryLevel: "heavy"},
		},
		{
			name: "keyvals 1",
			args: args{
				mapline: `<mapheader> <z>15</z> <x>10</x> <y>5</y> <n>zname</n> alwaysdark=true outdoor=true </mapheader>`,
			},
			want: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZCoord: 15, XOffset: 10, YOffset: 5,
				ZName: "zname", AlwaysDark: true, Outdoor: true},
		},
		{
			name: "keyvals 2",
			args: args{
				mapline: `<mapheader> <z>15</z> <x>10</x> <y>5</y> <n>zname</n> norecall=true townlimits=true </mapheader>`,
			},
			want: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZCoord: 15, XOffset: 10, YOffset: 5,
				ZName: "zname", TownLimits: true, NoRecall: true},
		},
		{
			name: "Invalid XML header",
			args: args{
				mapline: `<x>0</x> <y>0</n> <n>onecell</y>`,
			},
			want:    MapHeader{},
			wantErr: xml.UnmarshalError("expected element type <mapheader> but have <x>"),
		},
		{
			name: "Missing name",
			args: args{
				mapline: `<mapheader> <z>15</z> <x>0</x> <y>0</y> alwaysdark=true </mapheader>`,
			},
			want: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZCoord: 15, XOffset: 0, YOffset: 0,
				AlwaysDark: true},
			wantErr: errors.New("no z name"),
		},
		{
			name: "bad keyval",
			args: args{
				mapline: `<mapheader> <z>15</z> <x>10</x> <y>5</y> <n>zname</n> notreal=true townlimits=true </mapheader>`,
			},
			want: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZCoord: 15, XOffset: 10, YOffset: 5,
				ZName: "zname"},
			wantErr: errors.New("Unknown key: notreal"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := processXMLHeader(tt.args.mapline, MapHeader{})
			if (err != nil && tt.wantErr != nil && err.Error() != tt.wantErr.Error()) ||
				(err == nil && tt.wantErr != nil) {
				t.Errorf("processXMLHeader() err = %v, want %v", err, tt.wantErr)
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("processXMLHeader() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_mapStyleMapHeader(t *testing.T) {
	type args struct {
		mapHeader MapHeader
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "minimal",
			args: args{
				mapHeader: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZCoord: 15, XOffset: 10, YOffset: 5,
					ZName: "zname"},
			},
			want: `<z>15</z> <x>10</x> <y>5</y> <n>zname</n>` + "\n",
		},
		{
			name: "forestry",
			args: args{
				mapHeader: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZCoord: 15, XOffset: 10, YOffset: 5,
					ZName: "zname", ForestryLevel: "heavy"},
			},
			want: `<z>15</z> <x>10</x> <y>5</y> <f>heavy</f> <n>zname</n>` + "\n",
		},
		{
			name: "keyvals 1",
			args: args{
				mapHeader: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZCoord: 15, XOffset: 10, YOffset: 5,
					ZName: "zname", AlwaysDark: true, Outdoor: true},
			},
			want: `<z>15</z> <x>10</x> <y>5</y> <n>zname</n> alwaysdark=true outdoor=true` + "\n",
		},
		{
			name: "keyvals 2",
			args: args{
				mapHeader: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZCoord: 15, XOffset: 10, YOffset: 5,
					ZName: "zname", TownLimits: true, NoRecall: true},
			},
			want: `<z>15</z> <x>10</x> <y>5</y> <n>zname</n> norecall=true townlimits=true` + "\n",
		},
		{
			name: "comments",
			args: args{
				mapHeader: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZCoord: 15, XOffset: 10, YOffset: 5,
					ZName: "zname", Comments: []string{"comment 1", " comment 2"}},
			},
			want: strings.Join([]string{`//comment 1`, `// comment 2`, `<z>15</z> <x>10</x> <y>5</y> <n>zname</n>`}, "\n") +
				"\n",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := mapStyleMapHeader(tt.args.mapHeader); got != tt.want {
				t.Errorf("mapStyleMapHeader() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_mapHeaderRoundTrip1(t *testing.T) {
	tests := []struct {
		name    string
		mapline string
	}{
		{
			name:    "minimal",
			mapline: `<z>15</z> <x>10</x> <y>5</y> <n>zname</n>`,
		},
		{
			name:    "forestry",
			mapline: `<z>15</z> <x>10</x> <y>5</y> <f>heavy</f> <n>zname</n>`,
		},
		{
			name:    "keyvals 1",
			mapline: `<z>15</z> <x>10</x> <y>5</y> <n>zname</n> alwaysdark=true outdoor=true`,
		},
		{
			name:    "keyvals 2",
			mapline: `<z>15</z> <x>10</x> <y>5</y> <n>zname</n> norecall=true townlimits=true`,
		},
		{
			name:    "maximal",
			mapline: `<z>-12345</z> <x>2345</x> <y>-77</y> <f>mostly berries</f> <n>this is a zname</n> alwaysdark=true norecall=true outdoor=true townlimits=true`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			parsed, err := processXMLHeader(`<mapheader> `+tt.mapline+` </mapheader>`, MapHeader{})
			if err != nil {
				t.Errorf("processXMLHeader() err = %v", err)
			}
			got := mapStyleMapHeader(parsed)
			want := tt.mapline + "\n"
			if !reflect.DeepEqual(got, want) {
				t.Errorf("mapStyleMapHeader(processXMLHeader()) = %v, want %v", got, want)
			}
		})
	}
}

func Test_mapHeaderRoundTrip2(t *testing.T) {
	tests := []struct {
		name   string
		header MapHeader
	}{
		{
			name: "minimal",
			header: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZCoord: 15, XOffset: 10, YOffset: 5,
				ZName: "zname"},
		},
		{
			name: "forestry",
			header: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZCoord: 15, XOffset: 10, YOffset: 5,
				ZName: "zname", ForestryLevel: "heavy"},
		},
		{
			name: "keyvals 1",
			header: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZCoord: 15, XOffset: 10, YOffset: 5,
				ZName: "zname", AlwaysDark: true, Outdoor: true},
		},
		{
			name: "keyvals 2",
			header: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZCoord: 15, XOffset: 10, YOffset: 5,
				ZName: "zname", TownLimits: true, NoRecall: true},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mapline := mapStyleMapHeader(tt.header)
			got, err := processXMLHeader(`<mapheader> `+mapline+` </mapheader>`, MapHeader{})
			if err != nil {
				t.Errorf("processXMLHeader() err = %v", err)
			}
			if !reflect.DeepEqual(got, tt.header) {
				t.Errorf("processXMLHeader(mapStyleMapHeader()) = %v, want %v", got, tt.header)
			}
		})
	}
}

func Fuzz_MapHeaderRoundTrip1(f *testing.F) {
	testcases := []string{
		`<z>15</z> <x>10</x> <y>5</y> <n>zname</n>`,
		`<z>15</z> <x>10</x> <y>5</y> <f>heavy</f> <n>zname</n>`,
		`<z>15</z> <x>10</x> <y>5</y> <n>zname</n> alwaysdark=true outdoor=true`,
		`<z>15</z> <x>10</x> <y>5</y> <n>zname</n> norecall=true townlimits=true`,
		`<z>-12345</z> <x>2345</x> <y>-77</y>  <f>mostly berries</f> <n>this is a zname</n> alwaysdark=true norecall=true outdoor=true townlimits=true`,
	}

	for _, tc := range testcases {
		f.Add(tc)
	}

	skippedTests := 0
	goodXML := regexp.MustCompile(`^<z>(0|-?[1-9][0-9]*)</z> <x>(0|-?[1-9][0-9]*)</x> <y>(0|-?[1-9][0-9]*)</y>` +
		`( <f>[^<]+</f>)? <n>[^<]+</n>` +
		`( alwaysdark=true)?` + `( norecall=true)?` + `( outdoor=true)?` + `( townlimits=true)?` +
		`$`)

	f.Fuzz(func(t *testing.T, orig string) {

		if !goodXML.MatchString(orig) {
			skippedTests++
			t.Skip()
		}
		if strings.Contains(orig, "\r") {
			skippedTests++
			t.Skip()
		}
		if strings.Contains(orig, "&") {
			skippedTests++
			t.Skip()
		}

		mapHeader, err := processXMLHeader("<mapheader> "+orig+" </mapheader>", MapHeader{})
		if err != nil {
			skippedTests++
			t.Skip()
		}
		got := mapStyleMapHeader(mapHeader)
		if got != orig+"\n" {
			t.Errorf("mapStyleMapHeader(processXMLHeader()) = %v, want %v", got, orig)
		}
	})

	fmt.Printf("Tests skipped: %d\n", skippedTests)
}

func Fuzz_MapHeaderRoundTrip2b(f *testing.F) {
	testcases := []MapHeader{
		{
			XMLName: xml.Name{Space: "", Local: "mapheader"}, ZCoord: 15, XOffset: 10, YOffset: 5, ZName: "zname",
		},
		{
			XMLName: xml.Name{Space: "", Local: "mapheader"}, ZCoord: 15, XOffset: 10, YOffset: 5, ZName: "zname",
			ForestryLevel: "heavy",
		},
		{
			XMLName: xml.Name{Space: "", Local: "mapheader"}, ZCoord: 15, XOffset: 10, YOffset: 5, ZName: "zname",
			AlwaysDark: true, Outdoor: true,
		},
		{
			XMLName: xml.Name{Space: "", Local: "mapheader"}, ZCoord: 15, XOffset: 10, YOffset: 5, ZName: "zname",
			TownLimits: true, NoRecall: true,
		},
		{
			XMLName: xml.Name{Space: "", Local: "mapheader"}, ZCoord: -300, XOffset: 99, YOffset: -500, ZName: "1a2b3c4d",
			AlwaysDark: true, Outdoor: true, TownLimits: true, NoRecall: true,
		},
	}

	for _, tc := range testcases {
		f.Add(tc.ZCoord, tc.XOffset, tc.YOffset, tc.ZName, tc.ForestryLevel, tc.AlwaysDark, tc.Outdoor, tc.TownLimits, tc.NoRecall)
	}

	skippedTests := 0

	f.Fuzz(func(t *testing.T, zCoord, xOffset, yOffset int, zName, forestryLevel string, alwaysDark, outdoor, townLimits, noRecall bool) {
		mh := MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZCoord: zCoord, XOffset: xOffset, YOffset: yOffset,
			ZName: zName, ForestryLevel: forestryLevel, AlwaysDark: alwaysDark, Outdoor: outdoor, TownLimits: townLimits,
			NoRecall: noRecall}

		if mh.ZName == "" {
			// Invalid header, zname is blank.
			skippedTests++
			t.Skip()
		}

		var safeZName bytes.Buffer
		err := xml.EscapeText(&safeZName, []byte(mh.ZName))
		if err != nil || safeZName.String() != mh.ZName {
			// ZName isn't XML-safe
			skippedTests++
			t.Skip()
		}

		var safeForestryLevel bytes.Buffer
		err = xml.EscapeText(&safeForestryLevel, []byte(mh.ForestryLevel))
		// ForestryLevel isn't XML-safe
		if err != nil || safeForestryLevel.String() != mh.ForestryLevel {
			skippedTests++
			t.Skip()
		}

		mapline := mapStyleMapHeader(mh)
		got, err := processXMLHeader("<mapheader> "+mapline+" </mapheader>", MapHeader{})
		if err != nil {
			t.Errorf("processXMLHeader() err = %v", err)
		}
		if !reflect.DeepEqual(got, mh) {
			t.Errorf("processXMLHeader(mapStyleMapHeader()) = %v, want %v", got, mh)
		}
	})

	fmt.Printf("Tests skipped: %d\n", skippedTests)
}

func Test_SplitMapLevels(t *testing.T) {
	type args struct {
		buffer string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "empty",
			args: args{buffer: ""},
			want: []string{},
		},
		{
			name: "one minimal level",
			args: args{buffer: strings.Join([]string{`<z>15</z> <x>10</x> <y>5</y> <n>zname</n>`, `. `}, "\n")},
			want: []string{"<z>15</z> <x>10</x> <y>5</y> <n>zname</n>\n. "},
		},
		{
			name: "one minimal level with comments",
			args: args{buffer: strings.Join([]string{`//comment1a comment1b`, `//comment2`,
				`<z>15</z> <x>10</x> <y>5</y> <n>zname</n>`, `. `}, "\n")},
			want: []string{"//comment1a comment1b\n//comment2\n<z>15</z> <x>10</x> <y>5</y> <n>zname</n>\n. "},
		},
		{
			name: "ignore empty lines",
			args: args{buffer: strings.Join([]string{``, `<z>15</z> <x>10</x> <y>5</y> <n>zname</n>`, ``, `. `, ``}, "\n")},
			want: []string{"<z>15</z> <x>10</x> <y>5</y> <n>zname</n>\n. "},
		},
		{
			name: "two minimal levels",
			args: args{buffer: strings.Join([]string{
				`//comment1`, `<z>15</z> <x>10</x> <y>5</y> <n>zname1</n>`, `. `,
				`//comment2`, `<z>20</z> <x>10</x> <y>5</y> <n>zname2</n>`, `[]`,
			}, "\n")},
			want: []string{
				"//comment1\n<z>15</z> <x>10</x> <y>5</y> <n>zname1</n>\n. ",
				"//comment2\n<z>20</z> <x>10</x> <y>5</y> <n>zname2</n>\n[]",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SplitMapLevels(tt.args.buffer); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SplitMapLevels() = %v (len %v)(type %T), want %v (len %v)(type %T)",
					got, len(got), got, tt.want, len(tt.want), tt.want)
			}
		})
	}
}

func Test_mapToString(t *testing.T) {
	type args struct {
		mapHeader   MapHeader
		mapContents MapContents
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "One cell at origin",
			args: args{
				mapHeader: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZName: "onecell", Outdoor: false,
					NoRecall: false, AlwaysDark: false, TownLimits: false},
				mapContents: MapContents{
					Cells: map[CellKey]Cell{
						{0, 0, 0}: {"[]"},
					},
				},
			},
			want: strings.Join([]string{`<z>0</z> <x>0</x> <y>0</y> <n>onecell</n>`, `[]`}, "\n") + "\n",
		},
		{
			name: "One cell at origin with key values 1",
			args: args{
				mapHeader: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZName: "onecell", Outdoor: true,
					NoRecall: true, AlwaysDark: false, TownLimits: false},
				mapContents: MapContents{
					Cells: map[CellKey]Cell{
						{0, 0, 0}: {"[]"},
					},
				},
			},
			want: strings.Join([]string{
				`<z>0</z> <x>0</x> <y>0</y> <n>onecell</n> norecall=true outdoor=true`,
				`[]`}, "\n") + "\n",
		},
		{
			name: "One cell at origin with key values 2",
			args: args{
				mapHeader: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZName: "onecell", Outdoor: false,
					NoRecall: false, AlwaysDark: true, TownLimits: true},
				mapContents: MapContents{
					Cells: map[CellKey]Cell{
						{0, 0, 0}: {"[]"},
					},
				},
			},
			want: strings.Join([]string{
				`<z>0</z> <x>0</x> <y>0</y> <n>onecell</n> alwaysdark=true townlimits=true`,
				`[]`}, "\n") + "\n",
		},
		{
			name: "One cell at origin with comments",
			args: args{
				mapHeader: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZName: "onecell", Outdoor: false,
					NoRecall: false, AlwaysDark: false, TownLimits: false, Comments: []string{"comment1", "comment2"}},
				mapContents: MapContents{
					Cells: map[CellKey]Cell{
						{0, 0, 0}: {"[]"},
					},
				},
			},
			want: strings.Join([]string{`//comment1`, `//comment2`, `<z>0</z> <x>0</x> <y>0</y> <n>onecell</n>`, `[]`}, "\n") + "\n",
		},
		{
			name: "Multiple lines of cells at origin",
			args: args{
				mapHeader: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZName: "multilinescells", Outdoor: false,
					NoRecall: false, AlwaysDark: false, TownLimits: false}, mapContents: MapContents{
					Cells: map[CellKey]Cell{
						{0, 0, 0}: {"[]"},
						{1, 0, 0}: {"[]"},
						{2, 0, 0}: {"[]"},
						{3, 0, 0}: {"[]"},
						{0, 3, 0}: {"[]"},
						{1, 3, 0}: {"[]"},
						{2, 3, 0}: {"[]"},
						{3, 3, 0}: {"[]"},
						{0, 1, 0}: {"[]"},
						{1, 1, 0}: {". "},
						{2, 1, 0}: {". "},
						{3, 1, 0}: {"[]"},
						{0, 2, 0}: {"[]"},
						{1, 2, 0}: {". "},
						{2, 2, 0}: {". "},
						{3, 2, 0}: {"[]"},
					},
				},
			},
			want: strings.Join([]string{
				`<z>0</z> <x>0</x> <y>0</y> <n>multilinescells</n>`,
				`[][][][]`,
				`[]. . []`,
				`[]. . []`,
				`[][][][]`}, "\n") + "\n",
		},
		{
			name: "Multiple lines of cells with offsets and forestry",
			args: args{
				mapHeader: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZName: "multilinescells", Outdoor: false,
					NoRecall: false, AlwaysDark: false, TownLimits: false, ForestryLevel: "light",
					XOffset: 3, YOffset: 4, ZCoord: 5}, mapContents: MapContents{
					Cells: map[CellKey]Cell{
						{3, 4, 5}: {"[]"},
						{4, 4, 5}: {"[]"},
						{5, 4, 5}: {"[]"},
						{6, 4, 5}: {"[]"},
						{3, 7, 5}: {"[]"},
						{4, 7, 5}: {"[]"},
						{5, 7, 5}: {"[]"},
						{6, 7, 5}: {"[]"},
						{3, 5, 5}: {"[]"},
						{4, 5, 5}: {"{}"},
						{5, 5, 5}: {"{}"},
						{6, 5, 5}: {"[]"},
						{3, 6, 5}: {"[]"},
						{4, 6, 5}: {"{}"},
						{5, 6, 5}: {"{}"},
						{6, 6, 5}: {"[]"},
					},
				},
			},
			want: strings.Join([]string{
				`<z>5</z> <x>3</x> <y>4</y> <f>light</f> <n>multilinescells</n>`,
				`[][][][]`,
				`[]{}{}[]`,
				`[]{}{}[]`,
				`[][][][]`}, "\n") + "\n",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MapToString(tt.args.mapHeader, tt.args.mapContents); got != tt.want {
				t.Errorf("mapToString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_parseMapLevel(t *testing.T) {
	type args struct {
		levelBuffer string
	}
	tests := []struct {
		name    string
		args    args
		want    MapHeader
		want1   MapContents
		wantErr error
	}{
		{
			name: "One cell at origin",
			args: args{
				levelBuffer: `<z>0</z> <x>0</x> <y>0</y> <n>onecell</n>` + "\n" + `[]`,
			},
			want: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZName: "onecell", Outdoor: false, NoRecall: false, AlwaysDark: false, TownLimits: false},
			want1: MapContents{
				Cells: map[CellKey]Cell{
					{0, 0, 0}: {"[]"},
				},
			},
		},
		{
			name: "One line of cells at origin",
			args: args{
				levelBuffer: `<z>0</z> <x>0</x> <y>0</y> <n>onelinecells</n>` + "\n" + `[]. . []`,
			},
			want: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZName: "onelinecells", Outdoor: false,
				NoRecall: false, AlwaysDark: false, TownLimits: false},
			want1: MapContents{
				Cells: map[CellKey]Cell{
					{0, 0, 0}: {"[]"},
					{1, 0, 0}: {". "},
					{2, 0, 0}: {". "},
					{3, 0, 0}: {"[]"},
				},
			},
		},
		{
			name: "Multi lines of cells at origin",
			args: args{
				levelBuffer: `<z>0</z> <x>0</x> <y>0</y> <n>multilinescells</n>` + "\n" +
					`[][][][]` + "\n" +
					`[]. . []` + "\n" +
					`[]. . []` + "\n" +
					`[][][][]`,
			},
			want: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZName: "multilinescells", Outdoor: false,
				NoRecall: false, AlwaysDark: false, TownLimits: false},
			want1: MapContents{
				Cells: map[CellKey]Cell{
					{0, 0, 0}: {"[]"},
					{1, 0, 0}: {"[]"},
					{2, 0, 0}: {"[]"},
					{3, 0, 0}: {"[]"},
					{0, 3, 0}: {"[]"},
					{1, 3, 0}: {"[]"},
					{2, 3, 0}: {"[]"},
					{3, 3, 0}: {"[]"},
					{0, 1, 0}: {"[]"},
					{1, 1, 0}: {". "},
					{2, 1, 0}: {". "},
					{3, 1, 0}: {"[]"},
					{0, 2, 0}: {"[]"},
					{1, 2, 0}: {". "},
					{2, 2, 0}: {". "},
					{3, 2, 0}: {"[]"},
				},
			},
		},
		{
			name: "One cell at origin with comment",
			args: args{
				levelBuffer: `//comment` + "\n" + `<z>0</z> <x>0</x> <y>0</y> <n>onecell</n>` + "\n" + `[]`,
			},
			want: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZName: "onecell",
				Outdoor: false, NoRecall: false, AlwaysDark: false, TownLimits: false,
				Comments: []string{"comment"}},
			want1: MapContents{
				Cells: map[CellKey]Cell{
					{0, 0, 0}: {"[]"},
				},
			},
		},
		{
			name: "One cell at origin with comments",
			args: args{
				levelBuffer: `//comment1` + "\n" + `//comment2` + "\n" + `<z>0</z> <x>0</x> <y>0</y> <n>onecell</n>` + "\n" + `[]`,
			},
			want: MapHeader{XMLName: xml.Name{Space: "", Local: "mapheader"}, ZName: "onecell",
				Outdoor: false, NoRecall: false, AlwaysDark: false, TownLimits: false,
				Comments: []string{"comment1", "comment2"}},
			want1: MapContents{
				Cells: map[CellKey]Cell{
					{0, 0, 0}: {"[]"},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, err := ParseMapLevel(tt.args.levelBuffer)
			// fmt.Printf("parseMapLevel() got = %v %v %v\n", got, got1, err)
			if err != nil {
				t.Errorf("parseMapLevel() got error = %v, want %v", err, tt.wantErr)
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseMapLevel() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("parseMapLevel() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func Test_readAndWriteMap(t *testing.T) {
	type args struct {
		buffer string
	}
	tests := []struct {
		name string
		args args
		// no want; this should return what is passed to it unless something went wrong
		wantBad string
	}{
		{
			name: "two minimal levels",
			args: args{buffer: strings.Join([]string{
				`//comment1`, `<z>15</z> <x>10</x> <y>5</y> <n>zname1</n>`, `. `,
				`//comment2`, `<z>20</z> <x>10</x> <y>5</y> <n>zname2</n>`, `[]`,
			}, "\n") + "\n"},
		},
		{
			name: "two small levels",
			args: args{buffer: strings.Join([]string{
				`//comment1`, `<z>15</z> <x>10</x> <y>5</y> <n>zname1</n>`,
				`[][][]`,
				`[]. []`,
				`[][][]`,
				`//comment2`, `<z>20</z> <x>10</x> <y>5</y> <n>zname2</n>`,
				`/\/\/\`,
				`/\{}/\`,
				`/\/\/\`,
			}, "\n") + "\n"},
		},
		{
			name: "good and bad headers",
			args: args{buffer: strings.Join([]string{
				`//comment1`, `<z>15</z> <x>10</x> <y>5</y> <n>zname1</n>`, `. `,
				`//comment2`, `<z>20</y> <x>10</x> <y>5</z> <n>zname2</n>`, `[]`,
			}, "\n") + "\n"},
			wantBad: strings.Join([]string{
				`//comment1`, `<z>15</z> <x>10</x> <y>5</y> <n>zname1</n>`, `. `,
				`//XML syntax error on line 1: element <z> closed by </y>`,
				`<z>0</z> <x>0</x> <y>0</y> <n></n>`, ``,
			}, "\n") + "\n",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := ReadAndWriteMap(tt.args.buffer)
			if tt.wantBad != "" {
				if got != tt.wantBad {
					t.Errorf("readAndWriteMap() = %v, want %v", got, tt.wantBad)
				}
				return
			}
			if !reflect.DeepEqual(got, tt.args.buffer) {
				t.Errorf("readAndWriteMap() = %v, want %v", got, tt.args.buffer)
			}
		})
	}
}

func Fuzz_readAndWriteMap(f *testing.F) {
	testcases := []string{
		strings.Join([]string{
			`//comment1`, `<z>15</z> <x>10</x> <y>5</y> <n>zname1</n>`, `. `,
			`//comment2`, `<z>20</z> <x>10</x> <y>5</y> <n>zname2</n>`, `[]`,
		}, "\n") + "\n",
		strings.Join([]string{
			`//comment1`, `<z>15</z> <x>10</x> <y>5</y> <n>zname1</n>`,
			`[][][]`,
			`[]. []`,
			`[][][]`,
			`//comment2`, `<z>20</z> <x>10</x> <y>5</y> <n>zname2</n>`,
			`/\/\/\`,
			`/\{}/\`,
			`/\/\/\`,
		}, "\n") + "\n",
		strings.Join([]string{
			`//comment1`, `<z>15</z> <x>10</x> <y>5</y> <n>zname1</n>`,
			`[][][]`,
			`[]. []`,
			`[][][]`,
			`//comment2`, `<z>20</z> <x>10</x> <y>5</y> <n>zname2</n>`,
			`/\/\/\`,
			`/\{}/\`,
			`/\/\/\`,
			`//comment3`, `<z>-100</z> <x>5</x> <y>30</y> <n>zname 3</n>`,
			`/\[]/\`,
			`/\. /\`,
			`/\{}/\`,
		}, "\n") + "\n",
		"Annwn.txt",
		"Axe Glacier.txt",
		"Eridu.txt",
		"Hell.txt",
		"Island of Kesmai.txt",
		"Leng.txt",
		"Oakvael.txt",
		"Praetoseba.txt",
		"Rift Glacier.txt",
		"Shukumei.txt",
		"Torii.txt",
		"Underkingdom.txt",
		"test01.txt",
		"testunder.txt",
	}
	for _, tc := range testcases {
		if strings.HasSuffix(tc, ".txt") {
			buffer, err := os.ReadFile("../../testdata/maps/" + tc)
			if err != nil {
				tc = err.Error()
			} else {
				tc = strings.ReplaceAll(string(buffer), "\r", "\n")
				tc = strings.ReplaceAll(tc, "\n\n", "\n")
			}
		}
		f.Add(tc) // Use f.Add to provide a seed corpus
	}
	skippedTests := 0
	f.Fuzz(func(t *testing.T, orig string) {
		if shouldSkip(orig) {
			// fmt.Printf("Skipping %s\n", orig)
			skippedTests++
			t.Skip()
		}
		// fmt.Printf("Not skipped: %q\n", orig)
		written := ReadAndWriteMap(orig)
		if orig != written {
			t.Errorf("Before: %q, after: %q", orig, written)
		}
	})
	fmt.Printf("Tests skipped: %d\n", skippedTests)
}

func shouldSkip(s string) bool {
	// make sure it doesn't begin with a newline
	if strings.HasPrefix(s, "\n") {
		// fmt.Printf("Skipping - begins with newline.\n")
		return true
	}
	// make sure it ends with a single newline
	if !strings.HasSuffix(s, "\n") {
		// fmt.Printf("Skipping - %s ends with 0 newlines.\n", s)
		return true
	}

	// make sure it has no blank lines
	if strings.Contains(s, "\n\n") {
		// fmt.Printf("Skipping - %s contains blank lines.\n", s)
		return true
	}

	// make sure it has no carriage returns
	if strings.Contains(s, "\r") {
		// fmt.Printf("Skipping - %s contains carriage returns.\n", s)
		return true
	}

	goodXML := regexp.MustCompile(`^<z>(0|-?[1-9][0-9]*)</z> <x>(0|-?[1-9][0-9]*)</x> <y>(0|-?[1-9][0-9]*)</y>` +
		`( <f>[^<]+</f>)? <n>[^<]+</n>` +
		`( alwaysdark=true)?` + `( norecall=true)?` + `( outdoor=true)?` + `( townlimits=true)?` +
		`$`)
	sawHeader := false
	for _, line := range strings.Split(s, "\n") {
		if !strings.HasPrefix(line, "<") && !strings.HasPrefix(line, "//") && len(line)%2 != 0 {
			// fmt.Printf("Skipping - %s line %s has odd number of characters.\n", s, line)
			return true
		}
		// make sure XML is as expected
		if strings.HasPrefix(line, "<") {
			if !goodXML.MatchString(line) {
				// fmt.Printf("Skipping - %s XML line %s doesn't match our regex.\n", s, line)
				return true
			}
		}
		// No blank comments
		if strings.HasPrefix(line, "//") && len(line) == 2 {
			// fmt.Printf("Skipping - %s comment line %s has blank comment.\n", s, line)
			return true
		}

		// enforce order, comment(s) then header than cells
		if strings.HasPrefix(line, "//") {
			if sawHeader {
				// fmt.Printf("Skipping - %s comment line %s after header.\n", s, line)
				return true
			}
			continue
		}
		if strings.HasPrefix(line, "<") {
			if sawHeader {
				fmt.Printf("Skipping - %s has multiple headers.\n", s)
				return true
			}
			sawHeader = true
			continue
		}
		// must be cells:
		sawHeader = false
	}

	if len(SplitMapLevels(s)) == 0 {
		return true
	}
	// Split file into levels
	for _, levelBuffer := range SplitMapLevels(s) {
		mapHeader, mapContents, err := ParseMapLevel(levelBuffer)
		if err != nil {
			return true
		}
		if mapHeader.ZName == "" {
			return true
		}
		if len(mapContents.Cells) == 0 {
			return true
		}
	}

	return false
}

func Test_readFileToString(t *testing.T) {
	type args struct {
		filename string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{"file that doesn't exist", args{filename: "doesntexist"}, "", true},
		{"file that does exist", args{filename: "../../testdata/maps/Annwn.txt"},
			"//Doorway into Ydmos' tower is 1 way entrance", false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ReadFileToString(tt.args.filename)
			if (err != nil) != tt.wantErr {
				t.Errorf("readFileToString() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if strings.Split(got, "\r")[0] != tt.want {
				t.Errorf("readFileToString() = %v, want %v", got, tt.want)
			}
		})
	}
}
