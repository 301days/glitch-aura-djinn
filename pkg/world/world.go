package world

import (
	"encoding/xml"
	"errors"
	"os"
	"strconv"
	"strings"
)

type MapHeader struct {
	XMLName       xml.Name `xml:"mapheader"`
	ZCoord        int      `xml:"z"`
	ZName         string   `xml:"n"`
	XOffset       int      `xml:"x"`
	YOffset       int      `xml:"y"`
	ForestryLevel string   `xml:"f"`
	Outdoor       bool     `xml:"outdoor"`
	NoRecall      bool     `xml:"norecall"`
	AlwaysDark    bool     `xml:"alwaysdark"`
	TownLimits    bool     `xml:"townlimits"`
	Comments      []string `xml:"comment"`
	KVPairs       string   `xml:",chardata"`
}

type MapContents struct {
	Cells map[CellKey]Cell
}

type CellKey struct {
	X int
	Y int
	Z int
}

type Cell struct {
	Graphic string
}

// processXMLHeader processes an XML-like map header.
//
// It takes two parameters: xmlString, a string representing the XML content,
// and header, a MapHeader struct that will be populated with the parsed data.
//
// It returns a MapHeader struct and an error. The MapHeader struct contains
// the parsed data, while the error indicates whether there was an error
// during the parsing process. If an error occurs, the MapHeader struct will
// contain the partial results of the parsing process.
func processXMLHeader(xmlString string, header MapHeader) (MapHeader, error) {
	err := xml.Unmarshal([]byte(xmlString), &header)
	if err != nil {
		return header, err
	}

	parts := strings.Fields(header.KVPairs)
	header.KVPairs = "" // clear for return
	for _, part := range parts {
		keyValue := strings.Split(part, "=")
		if len(keyValue) == 2 {
			switch keyValue[0] {
			case "outdoor":
				header.Outdoor, _ = strconv.ParseBool(keyValue[1])
			case "norecall":
				header.NoRecall, _ = strconv.ParseBool(keyValue[1])
			case "alwaysdark":
				header.AlwaysDark, _ = strconv.ParseBool(keyValue[1])
			case "townlimits":
				header.TownLimits, _ = strconv.ParseBool(keyValue[1])
			default:
				return header, errors.New("Unknown key: " + keyValue[0])
			}
		}
	}

	if header.ZName == "" {
		return header, errors.New("no z name")
	}

	return header, nil
}

// mapStyleMapHeader generates a ascii map-style formatted string representation of the MapHeader
// object.
//
// It takes a MapHeader object as input and returns a string.
func mapStyleMapHeader(mapHeader MapHeader) string {
	var sb strings.Builder

	// Add comments
	for _, comment := range mapHeader.Comments {
		sb.WriteString("//" + comment + "\n")
	}

	// XML format
	sb.WriteString("<z>" + strconv.Itoa(mapHeader.ZCoord) + "</z> ")
	sb.WriteString("<x>" + strconv.Itoa(mapHeader.XOffset) + "</x> ")
	sb.WriteString("<y>" + strconv.Itoa(mapHeader.YOffset) + "</y> ")
	if mapHeader.ForestryLevel != "" {
		sb.WriteString("<f>" + mapHeader.ForestryLevel + "</f> ")
	}
	sb.WriteString("<n>" + mapHeader.ZName + "</n>")

	// Add key-value pairs
	kvs := []string{}
	if mapHeader.AlwaysDark {
		kvs = append(kvs, "alwaysdark=true")
	}
	if mapHeader.NoRecall {
		kvs = append(kvs, "norecall=true")
	}
	if mapHeader.Outdoor {
		kvs = append(kvs, "outdoor=true")
	}
	if mapHeader.TownLimits {
		kvs = append(kvs, "townlimits=true")
	}
	if len(kvs) > 0 {
		sb.WriteString(" ")
		sb.WriteString(strings.Join(kvs, " "))
	}

	sb.WriteString("\n")
	return sb.String()
}

// SplitMapLevels splits a map string into an array of level strings.
//
// Comment and header lines are kept with the subsequent level data.
//
// Parameters:
// - buffer: The map string to be split into levels.
//
// Return type:
// - []string: An array of strings representing the split levels.
func SplitMapLevels(buffer string) []string {
	lines := strings.Split(buffer, "\n")
	levelBuffers := []string{}
	var currentLevel string
	var isHeader = true

	for _, line := range lines {
		if len(line) == 0 {
			continue
		}
		isHeaderLine := len(line) > 0 && (strings.HasPrefix(line, "<") || strings.HasPrefix(line, "//"))
		if isHeaderLine && !isHeader {
			levelBuffers = append(levelBuffers, currentLevel)
			isHeader = true
			currentLevel = line
		} else {
			if currentLevel != "" {
				currentLevel += "\n"
			}
			currentLevel += line
			isHeader = isHeaderLine
		}
	}
	if currentLevel != "" {
		levelBuffers = append(levelBuffers, currentLevel)
	}
	return levelBuffers
}

// MapToString converts a map header and contents into a map level string.
//
// It takes a MapHeader struct and a MapContents struct as parameters. Using
// the offsets in the header, it iterates through the cells of the map and
// adds each cell to the map level string.
// It returns a string representing the map level as it appears in a map file.
func MapToString(header MapHeader, contents MapContents) string {
	sb := strings.Builder{}

	// Generate the map header string
	output := mapStyleMapHeader(header)
	sb.WriteString(output)

	// Initialize the starting coordinates
	x, y, z := header.XOffset-1, header.YOffset, header.ZCoord

	// Iterate through the cells and add each cell's graphic to the map string
	var cell Cell
	var exists bool
	for {
		x++
		cell, exists = contents.Cells[CellKey{x, y, z}]
		if !exists {
			sb.WriteRune('\n')
			y++
			x = header.XOffset
			cell, exists = contents.Cells[CellKey{x, y, z}]
			if !exists {
				break
			}
		}
		sb.WriteString(cell.Graphic)
	}

	return sb.String()
}

// ParseMapLevel parses a map level string and returns the header and contents.
//
// It takes a levelBuffer string as a parameter and returns a MapHeader, MapContents, and an error.
func ParseMapLevel(levelBuffer string) (MapHeader, MapContents, error) {
	var err error
	header := MapHeader{}
	contents := MapContents{
		Cells: map[CellKey]Cell{},
	}
	y := 0
	z := 0

	// Split the level buffer into lines
	lines := strings.Split(levelBuffer, "\n")
	for _, line := range lines {
		if strings.HasPrefix(line, "//") {
			// Append comment to header comments
			header.Comments = append(header.Comments, line[2:])
			continue
		}

		if strings.HasPrefix(line, "<") {
			// Process XML header - we initially have to wrap it so xml.Unmarshal
			//  can handle it.
			line = "<mapheader> " + line + " </mapheader>"
			header, err = processXMLHeader(line, header)
			if err != nil {
				return header, MapContents{}, err
			}
			y = header.YOffset
			z = header.ZCoord
			continue
		}

		// Parse cells and add to contents
		x := header.XOffset
		for j := 0; j < len(line); j += 2 {
			contents.Cells[CellKey{x, y, z}] = Cell{
				Graphic: line[j : j+2],
			}
			x++
		}
		y++
	}

	// Return parsed header and contents
	return header, contents, nil
}

// ReadAndWriteMap reads the buffer and processes each level, returning the rewritten levels as a string.
// If the buffer is properly formatted, it should return the same string.
//
// buffer: The input buffer containing the map data.
// Returns: The rewritten levels as a string.
func ReadAndWriteMap(buffer string) string {
	levels := SplitMapLevels(buffer)
	rewrittenLevels := ""
	for _, level := range levels {
		header, contents, err := ParseMapLevel(level)
		if err != nil {
			header = MapHeader{Comments: []string{err.Error()}}
			contents = MapContents{}
		}
		rewrittenLevels += MapToString(header, contents)
	}
	return rewrittenLevels
}

// mapLevelFromFile reads a map file and returns the header and contents of a specific level.
//
// It takes in the inputFilePath as a string which specifies the path to the map file.
// The inputLevelNumber is an integer that represents the specific level to be extracted.
//
// The function returns the map header and contents as world.MapHeader and world.MapContents respectively,
// along with an error if any occurs.
func MapLevelFromFile(inputFilePath string, inputLevelNumber int) (MapHeader, MapContents, error) {
	buffer, err := ReadFileToString(inputFilePath)
	if err != nil {
		panic(err)
	}
	buffer = strings.ReplaceAll(buffer, "\r", "\n")

	levels := SplitMapLevels(buffer)
	header, contents, err := ParseMapLevel(levels[inputLevelNumber])
	return header, contents, err
}

// ReadFileToString reads the content of a file and returns it as a string.
//
// It takes a `filename` parameter which is the path to the file to be read.
// It returns a `string` which is the content of the file, and an `error` if any error occurs.
func ReadFileToString(filename string) (string, error) {
	data, err := os.ReadFile(filename)
	if err != nil {
		return "", err
	}
	return string(data), nil
}

// stringToUint16 converts a two-rune string to a uint16.
//
// For performance reasons, map tiles can be used as uint16s instead of runes.
func StringToUint16(s string) uint16 {
	return uint16(s[1]) + uint16(s[0])<<8
}

// uint16ToString converts a uint16 to a two-rune string.
//
// For performance reasons, map tiles can be used as uint16s instead of runes.
func Uint16ToString(u uint16) string {
	return string([]rune{rune(u >> 8), rune(u & 0xff)})
}
