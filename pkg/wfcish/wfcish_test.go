package wfcish

import (
	"flag"
	"fmt"
	"io"
	"math/rand"
	"os"
	"reflect"
	"regexp"
	"strings"
	"testing"
	"time"

	"github.com/kylelemons/godebug/diff"
	"github.com/muesli/termenv"
	"gitlab.com/301days/glitch-aura-djinn/pkg/world"
)

var (
	maxLevelSize = flag.Int("max-level-size", 100, "Largest level size in characters for benchmarks")
)

var wholeLevelStrings = []string{
	`//comment1
<z>15</z> <x>10</x> <y>5</y> <n>zname1</n>
/\
//comment2
<z>20</z> <x>10</x> <y>5</y> <n>zname2</n>
[]
`,
	`//comment1
<z>15</z> <x>10</x> <y>5</y> <n>zname1</n>
[][][]
[]. []
[][][]
//comment2
<z>20</z> <x>10</x> <y>5</y> <n>zname2</n>
/\/\/\
/\{}/\
/\/\/\
`,
	`//comment1
<z>15</z> <x>10</x> <y>5</y> <n>zname1</n>
[][][]
[]. []
[][][]
//comment2
<z>20</z> <x>10</x> <y>5</y> <n>zname2</n>
/\/\/\
/\{}/\
/\/\/\
//comment3
<z>-100</z> <x>5</x> <y>30</y> <n>zname 3</n>
/\[]/\
/\. /\
/\{}/\
`,
	`//comment1
<z>15</z> <x>10</x> <y>5</y> <n>zname1</n>
  [][][]
  []. []
  [][][]
//comment2
<z>20</z> <x>10</x> <y>5</y> <n>zname2</n>
/\/\/\
/\{}/\
/\/\/\
//comment3
<z>-100</z> <x>5</x> <y>30</y> <n>zname 3</n>
/\[]/\
/\. /\
/\{}/\
`,
	"Annwn.txt",
	"Axe Glacier.txt",
	"Eridu.txt",
	"Hell.txt",
	"Island of Kesmai.txt",
	"Leng.txt",
	"Oakvael.txt",
	"Praetoseba.txt",
	"Rift Glacier.txt",
	"Shukumei.txt",
	"Torii.txt",
	"Underkingdom.txt",
	"test01.txt",
	"test02.txt",
	"testunder.txt",
}

type WholeLevelTestCase struct {
	name     string
	header   world.MapHeader
	contents world.MapContents
}

var displayCfg = DisplayCfg{
	Output:            termenv.NewOutput(io.Discard),
	FramesPerCollapse: 0,
	TimePerFrame:      time.Millisecond * 0,
}

func Fuzz_ApplyRules(f *testing.F) {
	testCases := []struct {
		origMap string
		x, y, z int
	}{
		{"Annwn.txt", 0, 0, 0},
		{"Axe Glacier.txt", 0, 0, 0},
		{"Eridu.txt", 0, 0, 0},
		{"Hell.txt", 0, 0, 0},
		{"Island of Kesmai.txt", 0, 0, 0},
		{"Leng.txt", 0, 0, 0},
		{"Oakvael.txt", 0, 0, 0},
		{"Praetoseba.txt", 0, 0, 0},
		{"Rift Glacier.txt", 0, 0, 0},
		{"Shukumei.txt", 0, 0, 0},
		{"Torii.txt", 0, 0, 0},
		{"Underkingdom.txt", 0, 0, 0},
		{"test01.txt", 0, 0, 0},
		{"testunder.txt", 0, 0, 0},
	}

	for _, tc := range testCases {
		if strings.HasSuffix(tc.origMap, ".txt") {
			buffer, err := os.ReadFile("../../testdata/maps/" + tc.origMap)
			if err != nil {
				tc.origMap = err.Error()
			} else {
				tc.origMap = strings.ReplaceAll(string(buffer), "\r", "\n")
				tc.origMap = strings.ReplaceAll(tc.origMap, "\n\n", "\n")
			}
		}
		f.Add(tc.origMap, tc.x, tc.y, tc.z) // Provide a seed corpus
	}
	skippedTests := 0
	f.Fuzz(func(t *testing.T, origMap string, x int, y int, z int) {
		if x < 0 || y < 0 || z < 0 || len(origMap) > 1000 || shouldSkip(origMap) {
			// fmt.Printf("Skipping %s\n", orig)
			skippedTests++
			t.Skip()
		}
		// fmt.Printf("Not skipped: %q\n", orig)

		levels := world.SplitMapLevels(origMap)
		for _, level := range levels {
			header, contents, err := world.ParseMapLevel(level)
			if err != nil {
				contents = world.MapContents{}
			}
			initialWeights := GatherTileWeights(contents.Cells)
			wContents := CreateWaveMapCollapseNulls(header, contents, initialWeights)
			ruleSet := DeriveRuleSetIgnoreNull(contents.Cells)
			SetEdges(wContents, ruleSet)
			wContents2 := newWaveMap()
			copyWaveMap(wContents2, wContents)
			if wContents.getCell(x, y, z) != nil {
				key := world.CellKey{X: x, Y: y, Z: z}
				rng := rand.New(rand.NewSource(0))
				CollapseCellWeighted(wContents, key, rng)
				err = ApplyRules(wContents, ruleSet, key)
				if err != nil {
					t.Errorf("ApplyRules 1 failed: %v", err)
				}
				rng = rand.New(rand.NewSource(0))
				CollapseCellWeighted(wContents2, key, rng)
				err = ApplyRules(wContents2, ruleSet, key)
				if err != nil {
					t.Errorf("ApplyRules 2 failed: %v", err)
				}
			} else {
				skippedTests++
				t.Skip()
			}
			for key := range contents.Cells {
				cell1 := wContents.getCell(key.X, key.Y, key.Z)
				cell2 := wContents2.getCell(key.X, key.Y, key.Z)
				if !reflect.DeepEqual(cell1, cell2) {
					t.Errorf("Contents differ for %v: %v vs %v", key, cell1, cell2)
				}
			}
		}
	})
	fmt.Printf("Tests skipped: %d\n", skippedTests)
}

func Fuzz_RSTM1(f *testing.F) {
	for _, tc := range wholeLevelStrings {
		if strings.HasSuffix(tc, ".txt") {
			buffer, err := os.ReadFile("../../testdata/maps/" + tc)
			if err != nil {
				tc = err.Error()
			} else {
				tc = strings.ReplaceAll(string(buffer), "\r", "\n")
				tc = strings.ReplaceAll(tc, "\n\n", "\n")
			}
		}
		f.Add(tc, 0) // Use f.Add to provide a seed corpus
	}
	skippedTests := 0
	f.Fuzz(func(t *testing.T, orig string, maplevel int) {
		if maplevel < 0 || len(orig) > 10000 || shouldSkip(orig) {
			// fmt.Printf("Skipping %s\n", orig)
			skippedTests++
			t.Skip()
		}
		// fmt.Printf("Not skipped: %q\n", orig)

		levels := world.SplitMapLevels(orig)
		if maplevel >= len(levels) {
			skippedTests++
			t.Skip()
		}
		header, contents, err := world.ParseMapLevel(levels[maplevel])
		if err != nil {
			contents = world.MapContents{}
		}
		rng := rand.New(rand.NewSource(0))
		displayCfg.FramesPerCollapse = 0
		displayCfg.Styled = false
		result1 := RSTM(header, contents, GatherTiles, CreateWaveMap, false, DeriveRuleSet,
			func(waveMap *WaveMapContents, rules RuleMap) {}, CollapseCell,
			func(waveMap *WaveMapContents, collapsedCell world.CellKey) error { return nil },
			RetryCfg{},
			displayCfg,
			rng)
		rng = rand.New(rand.NewSource(0))
		displayCfg.FramesPerCollapse = 1
		displayCfg.Styled = true
		result2 := RSTM(header, contents, GatherTiles, CreateWaveMap, false, DeriveRuleSet,
			func(waveMap *WaveMapContents, rules RuleMap) {}, CollapseCell,
			func(waveMap *WaveMapContents, collapsedCell world.CellKey) error { return nil },
			RetryCfg{},
			displayCfg,
			rng)

		if result1 != result2 {
			t.Errorf("Results differ: %s\n",
				diff.Diff(result1, result2))
		}
	})
	fmt.Printf("Tests skipped: %d\n", skippedTests)
}

func Fuzz_RSTM2(f *testing.F) {
	for _, tc := range wholeLevelStrings {
		if strings.HasSuffix(tc, ".txt") {
			buffer, err := os.ReadFile("../../testdata/maps/" + tc)
			if err != nil {
				tc = err.Error()
			} else {
				tc = strings.ReplaceAll(string(buffer), "\r", "\n")
				tc = strings.ReplaceAll(tc, "\n\n", "\n")
			}
		}
		f.Add(tc) // Use f.Add to provide a seed corpus
	}
	skippedTests := 0
	f.Fuzz(func(t *testing.T, orig string) {
		if shouldSkip(orig) {
			// fmt.Printf("Skipping %s\n", orig)
			skippedTests++
			t.Skip()
		}
		// fmt.Printf("Not skipped: %q\n", orig)

		levels := world.SplitMapLevels(orig)
		for _, level := range levels {
			if len(level) > 1000 {
				continue // Try to avoid very time-consuming levels
			}

			header, contents, err := world.ParseMapLevel(level)
			if err != nil {
				contents = world.MapContents{}
			}
			rng := rand.New(rand.NewSource(0))
			result1 := RSTM(header, contents,
				GatherTilesIgnoreNull,
				CreateWaveMap,
				true,
				DeriveRuleSetIgnoreNull,
				SetEdges,
				CollapseCell,
				func(waveMap *WaveMapContents, collapsedCell world.CellKey) error { return nil },
				RetryCfg{},
				displayCfg,
				rng)
			rng = rand.New(rand.NewSource(0))
			result2 := RSTM(header, contents,
				GatherTilesIgnoreNull,
				CreateWaveMap,
				true,
				DeriveRuleSetIgnoreNull,
				SetEdges,
				CollapseCell,
				func(waveMap *WaveMapContents, collapsedCell world.CellKey) error { return nil },
				RetryCfg{},
				displayCfg,
				rng)
			if result1 != result2 {
				t.Errorf("Results differ: %s\n",
					diff.Diff(result1, result2))
			}
		}
	})
	fmt.Printf("Tests skipped: %d\n", skippedTests)
}

func Fuzz_RSTM3(f *testing.F) {
	for _, tc := range wholeLevelStrings {
		if strings.HasSuffix(tc, ".txt") {
			buffer, err := os.ReadFile("../../testdata/maps/" + tc)
			if err != nil {
				tc = err.Error()
			} else {
				tc = strings.ReplaceAll(string(buffer), "\r", "\n")
				tc = strings.ReplaceAll(tc, "\n\n", "\n")
			}
		}
		f.Add(tc) // Use f.Add to provide a seed corpus
	}
	skippedTests := 0
	f.Fuzz(func(t *testing.T, orig string) {
		if shouldSkip(orig) {
			// fmt.Printf("Skipping %s\n", orig)
			skippedTests++
			t.Skip()
		}
		// fmt.Printf("Not skipped: %q\n", orig)

		levels := world.SplitMapLevels(orig)
		for _, level := range levels {
			if len(level) > 1000 {
				continue // Try to avoid very time-consuming levels
			}

			header, contents, err := world.ParseMapLevel(level)
			if err != nil {
				contents = world.MapContents{}
			}
			rng := rand.New(rand.NewSource(0))
			result1 := RSTM(header, contents,
				GatherTileWeights,
				CreateWaveMap,
				true,
				DeriveRuleSetIgnoreNull,
				SetEdges,
				CollapseCellWeighted,
				func(waveMap *WaveMapContents, collapsedCell world.CellKey) error { return nil },
				RetryCfg{},
				displayCfg,
				rng)
			rng = rand.New(rand.NewSource(0))
			result2 := RSTM(header, contents,
				GatherTileWeights,
				CreateWaveMap,
				true,
				DeriveRuleSetIgnoreNull,
				SetEdges,
				CollapseCellWeighted,
				func(waveMap *WaveMapContents, collapsedCell world.CellKey) error { return nil },
				RetryCfg{},
				displayCfg,
				rng)

			if result1 != result2 {
				t.Errorf("Results differ: %s\n",
					diff.Diff(result1, result2))
			}
		}
	})
	fmt.Printf("Tests skipped: %d\n", skippedTests)
}

func Fuzz_RSTM4(f *testing.F) {
	for _, tc := range wholeLevelStrings {
		if strings.HasSuffix(tc, ".txt") {
			buffer, err := os.ReadFile("../../testdata/maps/" + tc)
			if err != nil {
				tc = err.Error()
			} else {
				tc = strings.ReplaceAll(string(buffer), "\r", "\n")
				tc = strings.ReplaceAll(tc, "\n\n", "\n")
			}
		}
		for i := range world.SplitMapLevels(tc) {
			f.Add(tc, i) // Use f.Add to provide a seed corpus
		}
	}
	skippedTests := 0
	f.Fuzz(func(t *testing.T, orig string, levelNumber int) {
		if shouldSkip(orig) {
			// fmt.Printf("Skipping %s\n", orig)
			skippedTests++
			t.Skip()
		}
		// fmt.Printf("Not skipped: %q\n", orig)

		levels := world.SplitMapLevels(orig)
		if levelNumber < 0 || len(levels) <= levelNumber || len(levels[levelNumber]) > 1000 {
			skippedTests++
			t.Skip()
		}
		header, contents, err := world.ParseMapLevel(levels[levelNumber])
		if err != nil {
			contents = world.MapContents{}
		}
		rng := rand.New(rand.NewSource(0))
		displayCfg.FramesPerCollapse = 0
		displayCfg.Styled = false
		result1 := RSTM(header, contents,
			GatherTileWeights,
			CreateWaveMap,
			true,
			DeriveRuleSetIgnoreNull,
			SetEdges,
			CollapseCellWeighted,
			DecreaseWeights,
			RetryCfg{},
			displayCfg,
			rng)
		rng = rand.New(rand.NewSource(0))
		displayCfg.FramesPerCollapse = 1
		displayCfg.Styled = true
		result2 := RSTM(header, contents,
			GatherTileWeights,
			CreateWaveMap,
			true,
			DeriveRuleSetIgnoreNull,
			SetEdges,
			CollapseCellWeighted,
			DecreaseWeights,
			RetryCfg{},
			displayCfg,
			rng)

		if result1 != result2 {
			t.Errorf("Results differ: %s\n",
				diff.Diff(result1, result2))
		}
	})
	fmt.Printf("Tests skipped: %d\n", skippedTests)
}

func Test_RSTM(t *testing.T) {
	type args struct {
		levelMap           string
		getInitialWeights  func(cells map[world.CellKey]world.Cell) map[uint16]int
		getInitialWaveMap  func(header world.MapHeader, contents world.MapContents, initialWeights map[uint16]int, collapseNulls bool) *WaveMapContents
		getRuleSet         func(cells map[world.CellKey]world.Cell) RuleMap
		setWaveMapEdges    func(waveMap *WaveMapContents, rules RuleMap)
		collapseTheCell    func(waveMap *WaveMapContents, cellKey world.CellKey, rng *rand.Rand)
		decreaseTheWeights func(waveMap *WaveMapContents, collapsedCell world.CellKey) error
		retryCfg           RetryCfg
		displayCfg         DisplayCfg
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "empty map, nil functions",
			args: args{
				levelMap: "",
				getInitialWeights: func(cells map[world.CellKey]world.Cell) map[uint16]int {
					return map[uint16]int{}
				},
				getInitialWaveMap: func(header world.MapHeader, contents world.MapContents, initialWeights map[uint16]int, collapseNulls bool) *WaveMapContents {
					return newWaveMap()
				},
				getRuleSet: func(cells map[world.CellKey]world.Cell) RuleMap {
					return RuleMap{}
				},
				setWaveMapEdges: func(waveMap *WaveMapContents, rules RuleMap) {
				},
				collapseTheCell: func(waveMap *WaveMapContents, cellKey world.CellKey, rng *rand.Rand) {
				},
			},
			want: "<z>0</z> <x>0</x> <y>0</y> <n></n>\n\n",
		},
		// More test cases as needed.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			header, contents, err := world.ParseMapLevel(tt.args.levelMap)
			if err != nil {
				contents = world.MapContents{}
			}
			rng := rand.New(rand.NewSource(0))

			if got := RSTM(header, contents, tt.args.getInitialWeights, tt.args.getInitialWaveMap, false,
				tt.args.getRuleSet, tt.args.setWaveMapEdges, tt.args.collapseTheCell,
				tt.args.decreaseTheWeights, tt.args.retryCfg, tt.args.displayCfg, rng); got != tt.want {
				t.Errorf("RSTM() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_GatherTiles(t *testing.T) {
	type args struct {
		cells map[world.CellKey]world.Cell
	}
	tests := []struct {
		name string
		args args
		want map[uint16]int
	}{
		{
			name: "Empty map",
			args: args{cells: map[world.CellKey]world.Cell{}},
			want: map[uint16]int{},
		},
		{
			name: "Single cell",
			args: args{cells: map[world.CellKey]world.Cell{
				{X: 0, Y: 0, Z: 0}: {Graphic: "[]"},
			}},
			want: map[uint16]int{world.StringToUint16("[]"): 1},
		},
		{
			name: "Double cell",
			args: args{cells: map[world.CellKey]world.Cell{
				{X: 0, Y: 0, Z: 0}: {Graphic: "[]"},
				{X: 0, Y: 1, Z: 0}: {Graphic: "[]"},
			}},
			want: map[uint16]int{world.StringToUint16("[]"): 1},
		},
		{
			name: "Different cells",
			args: args{cells: map[world.CellKey]world.Cell{
				{X: 0, Y: 0, Z: 0}: {Graphic: "[]"},
				{X: 0, Y: 1, Z: 0}: {Graphic: " ."},
			}},
			want: map[uint16]int{world.StringToUint16("[]"): 1, world.StringToUint16(" ."): 1},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GatherTiles(tt.args.cells); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GatherTiles() = %T %v, want %T %v", got, got, tt.want, tt.want)
			}
		})
	}
}

func Test_DeriveRuleSet(t *testing.T) {
	type args struct {
		cells map[world.CellKey]world.Cell
	}
	tests := []struct {
		name string
		args args
		want RuleMap
	}{
		{
			name: "Empty map",
			args: args{cells: map[world.CellKey]world.Cell{}},
			want: RuleMap{},
		},
		{
			name: "Single cell",
			args: args{cells: map[world.CellKey]world.Cell{
				{X: 0, Y: 0, Z: 0}: {Graphic: "[]"},
			}},
			want: RuleMap{},
		},
		{
			name: "Double cell",
			args: args{cells: map[world.CellKey]world.Cell{
				{X: 0, Y: 0, Z: 0}: {Graphic: "[]"},
				{X: 0, Y: 1, Z: 0}: {Graphic: "[]"},
			}},
			want: RuleMap{23389: OffsetMap{{0, -1}: TileMap{23389: true}, {0, 1}: TileMap{23389: true}}},
		},
		{
			name: "Different cells",
			args: args{cells: map[world.CellKey]world.Cell{
				{X: 0, Y: 0, Z: 0}: {Graphic: "[]"},
				{X: 0, Y: 1, Z: 0}: {Graphic: " ."},
			}},
			want: RuleMap{0x202e: OffsetMap{Offset{X: 0, Y: -1}: TileMap{0x5b5d: true}}, 0x5b5d: OffsetMap{Offset{X: 0, Y: 1}: TileMap{0x202e: true}}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rules := DeriveRuleSet(tt.args.cells)
			if !reflect.DeepEqual(rules, tt.want) {
				t.Errorf("DeriveRuleSet() = %#v, want %#v", rules, tt.want)
			}
		})
	}
}

func Test_DeriveRuleSetIgnoreNull(t *testing.T) {
	type args struct {
		cells map[world.CellKey]world.Cell
	}
	tests := []struct {
		name string
		args args
		want RuleMap
	}{
		{
			name: "Empty map",
			args: args{cells: map[world.CellKey]world.Cell{}},
			want: RuleMap{},
		},
		{
			name: "Single cell",
			args: args{cells: map[world.CellKey]world.Cell{
				{X: 0, Y: 0, Z: 0}: {Graphic: "[]"},
			}},
			want: RuleMap{23389: OffsetMap{
				{-1, -1}: TileMap{8224: true}, {-1, 0}: TileMap{8224: true},
				{-1, 1}: {8224: true}, {0, -1}: {8224: true}, {0, 1}: {8224: true}, {1, -1}: {8224: true},
				{1, 0}: {8224: true}, {1, 1}: {8224: true},
			}},
		},
		{
			name: "Double cell",
			args: args{cells: map[world.CellKey]world.Cell{
				{X: 0, Y: 0, Z: 0}: {Graphic: "[]"},
				{X: 0, Y: 1, Z: 0}: {Graphic: "[]"},
			}},
			want: RuleMap{0x5b5d: OffsetMap{
				Offset{X: -1, Y: -1}: TileMap{0x2020: true},
				Offset{X: -1, Y: 0}:  TileMap{0x2020: true}, Offset{X: -1, Y: 1}: TileMap{0x2020: true},
				Offset{X: 0, Y: -1}: TileMap{0x2020: true, 0x5b5d: true},
				Offset{X: 0, Y: 1}:  TileMap{0x2020: true, 0x5b5d: true},
				Offset{X: 1, Y: -1}: TileMap{0x2020: true},
				Offset{X: 1, Y: 0}:  TileMap{0x2020: true}, Offset{X: 1, Y: 1}: TileMap{0x2020: true},
			}},
		},
		{
			name: "Different cells",
			args: args{cells: map[world.CellKey]world.Cell{
				{X: 0, Y: 0, Z: 0}: {Graphic: "[]"},
				{X: 0, Y: 1, Z: 0}: {Graphic: " ."},
			}},
			want: RuleMap{0x202e: OffsetMap{Offset{X: -1, Y: -1}: TileMap{0x2020: true}, Offset{X: -1, Y: 0}: TileMap{0x2020: true}, Offset{X: -1, Y: 1}: TileMap{0x2020: true}, Offset{X: 0, Y: -1}: TileMap{0x5b5d: true}, Offset{X: 0, Y: 1}: TileMap{0x2020: true}, Offset{X: 1, Y: -1}: TileMap{0x2020: true}, Offset{X: 1, Y: 0}: TileMap{0x2020: true}, Offset{X: 1, Y: 1}: TileMap{0x2020: true}}, 0x5b5d: OffsetMap{Offset{X: -1, Y: -1}: TileMap{0x2020: true}, Offset{X: -1, Y: 0}: TileMap{0x2020: true}, Offset{X: -1, Y: 1}: TileMap{0x2020: true}, Offset{X: 0, Y: -1}: TileMap{0x2020: true}, Offset{X: 0, Y: 1}: TileMap{0x202e: true}, Offset{X: 1, Y: -1}: TileMap{0x2020: true}, Offset{X: 1, Y: 0}: TileMap{0x2020: true}, Offset{X: 1, Y: 1}: TileMap{0x2020: true}}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rules := DeriveRuleSetIgnoreNull(tt.args.cells)
			if fmt.Sprint(tt.want) != fmt.Sprint(rules) {
				t.Errorf("DeriveRuleSetIgnoreNull() = %#v, want %#v", rules, tt.want)
			}
		})
	}
}

func shouldSkip(s string) bool {
	// make sure it doesn't begin with a newline
	if strings.HasPrefix(s, "\n") {
		// fmt.Printf("Skipping - begins with newline.\n")
		return true
	}
	// make sure it ends with a single newline
	if !strings.HasSuffix(s, "\n") {
		// fmt.Printf("Skipping - %s ends with 0 newlines.\n", s)
		return true
	}

	// make sure it has no blank lines
	if strings.Contains(s, "\n\n") {
		// fmt.Printf("Skipping - %s contains blank lines.\n", s)
		return true
	}

	// make sure it has no carriage returns
	if strings.Contains(s, "\r") {
		// fmt.Printf("Skipping - %s contains carriage returns.\n", s)
		return true
	}

	goodXML := regexp.MustCompile(`^<z>(0|-?[1-9][0-9]*)</z> <x>(0|-?[1-9][0-9]*)</x> <y>(0|-?[1-9][0-9]*)</y>` +
		`( <f>[^<]+</f>)? <n>[^<]+</n>` +
		`( alwaysdark=true)?` + `( norecall=true)?` + `( outdoor=true)?` + `( townlimits=true)?` +
		`$`)
	sawHeader := false
	for _, line := range strings.Split(s, "\n") {
		if !strings.HasPrefix(line, "<") && !strings.HasPrefix(line, "//") && len(line)%2 != 0 {
			// fmt.Printf("Skipping - %s line %s has odd number of characters.\n", s, line)
			return true
		}
		// make sure XML is as expected
		if strings.HasPrefix(line, "<") {
			if !goodXML.MatchString(line) {
				// fmt.Printf("Skipping - %s XML line %s doesn't match our regex.\n", s, line)
				return true
			}
		}
		// No blank comments
		if strings.HasPrefix(line, "//") && len(line) == 2 {
			// fmt.Printf("Skipping - %s comment line %s has blank comment.\n", s, line)
			return true
		}

		// enforce order, comment(s) then header than cells
		if strings.HasPrefix(line, "//") {
			if sawHeader {
				// fmt.Printf("Skipping - %s comment line %s after header.\n", s, line)
				return true
			}
			continue
		}
		if strings.HasPrefix(line, "<") {
			if sawHeader {
				fmt.Printf("Skipping - %s has multiple headers.\n", s)
				return true
			}
			sawHeader = true
			continue
		}
		// must be cells:
		sawHeader = false
	}

	// Split file into levels
	split := world.SplitMapLevels(s)
	for _, levelBuffer := range split {
		mapHeader, mapContents, err := world.ParseMapLevel(levelBuffer)
		if err != nil {
			return true
		}
		if mapHeader.ZName == "" {
			return true
		}
		if len(mapContents.Cells) == 0 {
			return true
		}
	}

	return false
}

func Test_SetEdges(t *testing.T) {
	type args struct {
		header  world.MapHeader
		waveMap *WaveMapContents
		rules   RuleMap
	}
	type TestCase struct {
		name string
		args args
	}
	tests := []TestCase{}

	for _, tc := range wholeLevelStrings {
		tcName := strings.Split(tc, "\n")[0]
		if strings.HasSuffix(tc, ".txt") {
			buffer, err := os.ReadFile("../../testdata/maps/" + tc)
			if err != nil {
				tc = err.Error()
			} else {
				tc = strings.ReplaceAll(string(buffer), "\r", "\n")
				tc = strings.ReplaceAll(tc, "\n\n", "\n")
			}
		}
		levels := world.SplitMapLevels(tc)
		for levelNum, level := range levels {
			header, contents, err := world.ParseMapLevel(level)
			if err != nil {
				contents = world.MapContents{}
			}
			initialWeights := GatherTileWeights(contents.Cells)
			wContents := CreateWaveMapCollapseNulls(header, contents, initialWeights)
			ruleSet := DeriveRuleSetIgnoreNull(contents.Cells)
			newCase := TestCase{name: fmt.Sprint(tcName, ", level: ", levelNum), args: args{header: header, waveMap: wContents, rules: ruleSet}}
			tests = append(tests, newCase)
		}
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			SetEdges(tt.args.waveMap, tt.args.rules)
			s := world.MapToString(tt.args.header, waveMapToContents(*tt.args.waveMap, false))
			if len(s) <= 0 {
				t.Errorf("After SetEdges(), wave map null")
			}
		})
	}
}

func Test_SetEdgesExtended(t *testing.T) {
	type args struct {
		header  world.MapHeader
		waveMap *WaveMapContents
		rules   RuleMap
	}
	type TestCase struct {
		name string
		args args
	}
	tests := []TestCase{}

	for _, tc := range wholeLevelStrings {
		tcName := strings.Split(tc, "\n")[0]
		if strings.HasSuffix(tc, ".txt") {
			buffer, err := os.ReadFile("../../testdata/maps/" + tc)
			if err != nil {
				tc = err.Error()
			} else {
				tc = strings.ReplaceAll(string(buffer), "\r", "\n")
				tc = strings.ReplaceAll(tc, "\n\n", "\n")
			}
		}
		levels := world.SplitMapLevels(tc)
		for levelNum, level := range levels {
			header, contents, err := world.ParseMapLevel(level)
			if err != nil {
				contents = world.MapContents{}
			}
			initialWeights := GatherTileWeights(contents.Cells)
			wContents := CreateWaveMapCollapseNulls(header, contents, initialWeights)
			ruleSet := DeriveRuleSetIgnoreNull(contents.Cells)
			newCase := TestCase{name: fmt.Sprint(tcName, ", level: ", levelNum), args: args{header: header, waveMap: wContents, rules: ruleSet}}
			tests = append(tests, newCase)
		}
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			wm1 := newWaveMap()
			copyWaveMap(wm1, tt.args.waveMap)
			wm2 := newWaveMap()
			copyWaveMap(wm2, tt.args.waveMap)
			SetEdgesExtended(wm1, tt.args.rules, 1)
			SetEdgesExtended(wm2, tt.args.rules, 1)
			if !reflect.DeepEqual(wm1, wm2) {
				t.Errorf("After SetEdgesExtended(), wm1 %v, after SetEdgesExtended(), wm2 %v", wm1, wm2)
			}
		})
	}
}

func Test_CollapseMap(t *testing.T) {
	type args struct {
		header  world.MapHeader
		waveMap *WaveMapContents
		rules   RuleMap
	}
	type TestCase struct {
		name string
		args args
	}
	tests := []TestCase{}

	for _, tc := range wholeLevelStrings {
		tcName := strings.Split(tc, "\n")[0]
		if strings.HasSuffix(tc, ".txt") {
			buffer, err := os.ReadFile("../../testdata/maps/" + tc)
			if err != nil {
				tc = err.Error()
			} else {
				tc = strings.ReplaceAll(string(buffer), "\r", "\n")
				tc = strings.ReplaceAll(tc, "\n\n", "\n")
			}
		}
		levels := world.SplitMapLevels(tc)
		for levelNum, level := range levels {
			if len(level) > 1000 {
				continue // Avoid time-consuming levels
			}
			header, contents, err := world.ParseMapLevel(level)
			if err != nil {
				contents = world.MapContents{}
			}
			initialWeights := GatherTileWeights(contents.Cells)
			wContents := CreateWaveMapCollapseNulls(header, contents, initialWeights)
			ruleSet := DeriveRuleSetExtended(contents.Cells, 1, true)
			newCase := TestCase{name: fmt.Sprint(tcName, ", level: ", levelNum), args: args{header: header, waveMap: wContents, rules: ruleSet}}
			tests = append(tests, newCase)
		}
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			wm1 := newWaveMap()
			copyWaveMap(wm1, tt.args.waveMap)
			wm2 := newWaveMap()
			copyWaveMap(wm2, tt.args.waveMap)

			SetEdgesExtended(wm1, tt.args.rules, 1)
			rng1 := rand.New(rand.NewSource(0))
			collapseMap(wm1, tt.args.rules, CollapseCellWeighted, ApplyRulesDirectional, DecreaseWeights, RetryCfg{MaxRetriesPerSeed: 1},
				DisplayCfg{}, tt.args.header, rng1)

			SetEdgesExtended(wm2, tt.args.rules, 1)
			rng2 := rand.New(rand.NewSource(0))
			collapseMapExtended(wm2, tt.args.rules, CollapseCellWeighted, ApplyRulesExtended, DecreaseWeights, 1, RetryCfg{MaxRetriesPerSeed: 1},
				DisplayCfg{FramesPerCollapse: 1, Output: termenv.NewOutput(io.Discard)}, tt.args.header, rng2)
			if !reflect.DeepEqual(wm1, wm2) {
				t.Errorf("After collapseMap(), wm1 %v, after collapseMap(), wm2 %v", wm1, wm2)
			}
		})
	}
}

func Test_XSTM1(t *testing.T) {
	tests := getWholeLevelTestCases()
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			origMap := world.MapToString(tt.header, tt.contents)

			map1 := XSTM(tt.header, tt.contents,
				GatherTiles,
				CreateWaveMap,
				false,
				DeriveRuleSetExtended,
				3,
				false,
				func(waveMap *WaveMapContents, rules RuleMap, distance int) {},
				CollapseCell,
				func(waveMap *WaveMapContents, collapsedCell world.CellKey) error { return nil },
				RetryCfg{},
				displayCfg,
				origMap)

			if len(map1) != len(origMap) {
				t.Errorf("After XSTM(), map1 (len %d) %v, after XSTM(), origMap (len %d) %v", len(map1), map1, len(origMap), origMap)
			}
		})
	}
}

func Benchmark_XSTM1(b *testing.B) {
	tests := getWholeLevelTestCases()
	for n := 0; n < b.N; n++ {
		for _, tt := range tests {
			origMap := world.MapToString(tt.header, tt.contents)

			map1 := XSTM(tt.header, tt.contents,
				GatherTiles,
				CreateWaveMap,
				false,
				DeriveRuleSetExtended,
				3,
				false,
				func(waveMap *WaveMapContents, rules RuleMap, distance int) {},
				CollapseCell,
				func(waveMap *WaveMapContents, collapsedCell world.CellKey) error { return nil },
				RetryCfg{},
				displayCfg,
				origMap)

			if len(map1) != len(origMap) {
				panic(fmt.Sprintf("After XSTM(), map1 (len %d) %v, after XSTM(), origMap (len %d) %v", len(map1), map1, len(origMap), origMap))
			}
		}
	}
}

func Test_XSTM2(t *testing.T) {
	tests := getWholeLevelTestCases()
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			origMap := world.MapToString(tt.header, tt.contents)
			displayCfg.FramesPerCollapse = 1
			displayCfg.Styled = true

			map2 := XSTM(tt.header, tt.contents,
				GatherTilesIgnoreNull,
				CreateWaveMap,
				true,
				DeriveRuleSetExtended,
				3,
				true,
				SetEdgesExtended,
				CollapseCell,
				func(waveMap *WaveMapContents, collapsedCell world.CellKey) error { return nil },
				RetryCfg{},
				displayCfg,
				origMap)
			displayCfg.FramesPerCollapse = 0
			displayCfg.Styled = false

			if len(map2) != len(origMap) {
				t.Errorf("After XSTM(), map2 (len %d) %v, after XSTM(), origMap (len %d) %v", len(map2), map2, len(origMap), origMap)
			}
		})
	}
}

func Benchmark_XSTM2(b *testing.B) {
	tests := getWholeLevelTestCases()
	for n := 0; n < b.N; n++ {
		for _, tt := range tests {
			origMap := world.MapToString(tt.header, tt.contents)
			displayCfg.FramesPerCollapse = 0
			displayCfg.Styled = true

			map2 := XSTM(tt.header, tt.contents,
				GatherTilesIgnoreNull,
				CreateWaveMap,
				true,
				DeriveRuleSetExtended,
				3,
				true,
				SetEdgesExtended,
				CollapseCell,
				func(waveMap *WaveMapContents, collapsedCell world.CellKey) error { return nil },
				RetryCfg{},
				displayCfg,
				origMap)
			displayCfg.FramesPerCollapse = 0
			displayCfg.Styled = false

			if len(map2) != len(origMap) {
				panic(fmt.Sprintf("After XSTM(), map2 (len %d) %v, after XSTM(), origMap (len %d) %v", len(map2), map2, len(origMap), origMap))
			}
		}
	}
}

func Test_XSTM3(t *testing.T) {
	tests := getWholeLevelTestCases()
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			origMap := world.MapToString(tt.header, tt.contents)
			displayCfg.FramesPerCollapse = 1
			displayCfg.Styled = true
			map3 := XSTM(tt.header, tt.contents,
				GatherTileWeights,
				CreateWaveMap,
				true,
				DeriveRuleSetExtended,
				3,
				true,
				SetEdgesExtended,
				CollapseCellWeighted,
				func(waveMap *WaveMapContents, collapsedCell world.CellKey) error { return nil },
				RetryCfg{},
				displayCfg,
				origMap)

			displayCfg.FramesPerCollapse = 0
			displayCfg.Styled = false

			if len(map3) != len(origMap) {
				t.Errorf("After XSTM(), map3 (len %d) %v, after XSTM(), origMap (len %d) %v", len(map3), map3, len(origMap), origMap)
			}
		})
	}
}

func Benchmark_XSTM3(b *testing.B) {
	tests := getWholeLevelTestCases()

	for n := 0; n < b.N; n++ {
		for _, tt := range tests {
			origMap := world.MapToString(tt.header, tt.contents)
			map3 := XSTM(tt.header, tt.contents,
				GatherTileWeights,
				CreateWaveMap,
				true,
				DeriveRuleSetExtended,
				3,
				true,
				SetEdgesExtended,
				CollapseCellWeighted,
				func(waveMap *WaveMapContents, collapsedCell world.CellKey) error { return nil },
				RetryCfg{},
				displayCfg,
				origMap)

			displayCfg.FramesPerCollapse = 0
			displayCfg.Styled = false

			if len(map3) != len(origMap) {
				panic(fmt.Sprintf("After XSTM(), map3 (len %d) %v, after XSTM(), origMap (len %d) %v", len(map3), map3, len(origMap), origMap))
			}
		}
	}
}

func Test_XSTM4(t *testing.T) {
	tests := getWholeLevelTestCases()

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			origMap := world.MapToString(tt.header, tt.contents)
			displayCfg.FramesPerCollapse = 1
			displayCfg.Styled = true
			map4 := XSTM(tt.header, tt.contents,
				GatherTileWeights,
				CreateWaveMap,
				true,
				DeriveRuleSetExtended,
				3,
				true,
				SetEdgesExtended,
				CollapseCellWeighted,
				DecreaseWeights,
				RetryCfg{},
				displayCfg,
				origMap)

			displayCfg.FramesPerCollapse = 0
			displayCfg.Styled = false

			if len(map4) != len(origMap) {
				t.Errorf("After XSTM(), map4 (len %d) %v, after XSTM(), origMap (len %d) %v", len(map4), map4, len(origMap), origMap)
			}
		})
	}
}

func Test_XSTM4Cache(t *testing.T) {
	tests := getWholeLevelTestCases()

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			origMap := world.MapToString(tt.header, tt.contents)
			displayCfg.FramesPerCollapse = 1
			displayCfg.Styled = true
			map4 := XSTM(tt.header, tt.contents,
				GatherTileWeights,
				CreateWaveMap,
				true,
				DeriveRuleSetExtended,
				3,
				true,
				SetEdgesExtended,
				CollapseCellWeighted,
				DecreaseWeights,
				RetryCfg{},
				displayCfg,
				origMap)
			badMap := XSTM(tt.header, tt.contents,
				GatherTileWeights,
				CreateWaveMap,
				true,
				DeriveRuleSetExtended,
				3,
				true,
				SetEdgesExtended,
				CollapseCellWeighted,
				DecreaseWeightsBadCache,
				RetryCfg{},
				displayCfg,
				origMap)

			displayCfg.FramesPerCollapse = 0
			displayCfg.Styled = false

			if map4 != badMap {
				t.Errorf("After XSTM(), map4 (len %d) %v, badMap (len %d) %v", len(map4), map4, len(badMap), badMap)
			}
		})
	}
}

func Benchmark_XSTM4BadCache(b *testing.B) {
	tests := getWholeLevelTestCases()

	for n := 0; n < b.N; n++ {
		for _, tt := range tests {
			origMap := world.MapToString(tt.header, tt.contents)
			map4 := XSTM(tt.header, tt.contents,
				GatherTileWeights,
				CreateWaveMap,
				true,
				DeriveRuleSetExtended,
				3,
				true,
				SetEdgesExtended,
				CollapseCellWeighted,
				DecreaseWeightsBadCache,
				RetryCfg{},
				displayCfg,
				origMap)

			if len(map4) != len(origMap) {
				panic(fmt.Sprintf("After XSTM(), map4 (len %d) %v, after XSTM(), origMap (len %d) %v", len(map4), map4, len(origMap), origMap))
			}
		}
	}
}

func Benchmark_XSTM4GoodCache(b *testing.B) {
	tests := getWholeLevelTestCases()

	for n := 0; n < b.N; n++ {
		for _, tt := range tests {
			origMap := world.MapToString(tt.header, tt.contents)
			map4 := XSTM(tt.header, tt.contents,
				GatherTileWeights,
				CreateWaveMap,
				true,
				DeriveRuleSetExtended,
				3,
				true,
				SetEdgesExtended,
				CollapseCellWeighted,
				DecreaseWeights,
				RetryCfg{},
				displayCfg,
				origMap)

			if len(map4) != len(origMap) {
				panic(fmt.Sprintf("After XSTM(), map4 (len %d) %v, after XSTM(), origMap (len %d) %v", len(map4), map4, len(origMap), origMap))
			}
		}
	}
}

func getWholeLevelTestCases() []WholeLevelTestCase {
	maxSize := *maxLevelSize
	tests := []WholeLevelTestCase{}
	for _, tc := range wholeLevelStrings {
		tcName := strings.Split(tc, "\n")[0]
		if strings.HasSuffix(tc, ".txt") {
			buffer, err := os.ReadFile("../../testdata/maps/" + tc)
			if err != nil {
				tc = err.Error()
			} else {
				tc = strings.ReplaceAll(string(buffer), "\r", "\n")
				tc = strings.ReplaceAll(tc, "\n\n", "\n")
			}
		}
		levels := world.SplitMapLevels(tc)
		for levelNum, level := range levels {
			if len(level) > maxSize {
				continue // Avoid time-consuming levels
			}
			header, contents, err := world.ParseMapLevel(level)
			if err != nil {
				contents = world.MapContents{}
			}
			newCase := WholeLevelTestCase{name: fmt.Sprint(tcName, ", level: ", levelNum), header: header, contents: contents}
			tests = append(tests, newCase)
		}
	}
	return tests
}
