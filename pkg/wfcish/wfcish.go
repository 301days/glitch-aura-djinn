package wfcish

import (
	"errors"
	"fmt"
	"math"
	"math/rand"
	"sort"
	"strings"
	"sync"
	"time"

	"github.com/charmbracelet/lipgloss"
	"github.com/muesli/termenv"
	"gitlab.com/301days/glitch-aura-djinn/pkg/world"
	"golang.org/x/exp/slices"
)

type Offset struct {
	X, Y int
}

type TileMap map[uint16]bool

type OffsetMap map[Offset]TileMap

// The RuleMap is keyed by the central tile, and contains a map of offsets to
// maps of neighbor tiles at that offset.
type RuleMap map[uint16]OffsetMap

// Coordinates in the wavemap start at 0; the offsets are used to translate to
// the world coordinates.
type WaveMapContents struct {
	cellArray                 []*SuperpositionCell
	TypicalCell               *SuperpositionCell
	XOffset, YOffset, ZOffset int
	Xdim, Ydim, Zdim          int
	minEntropyCache           float64
	minEntropyCacheValid      bool
}

var (
	log2CacheLock   sync.RWMutex
	nlog2nCacheLock sync.RWMutex
	log2cache       []float64
	nlog2ncache     []float64
)

func newWaveMap() *WaveMapContents {
	return &WaveMapContents{
		cellArray:   make([]*SuperpositionCell, 0),
		TypicalCell: &SuperpositionCell{},
	}
}

func (wm *WaveMapContents) getNumCells() int {
	num := 0
	for _, cell := range wm.cellArray {
		if cell != nil {
			num++
		}
	}
	return num
}

func (wm *WaveMapContents) setDimensions(header world.MapHeader, contents world.MapContents) {
	for key := range contents.Cells {
		if key.X-header.XOffset > wm.Xdim {
			wm.Xdim = key.X - header.XOffset
		}
		if key.Y-header.YOffset > wm.Ydim {
			wm.Ydim = key.Y - header.YOffset
		}
	}
	wm.Xdim++
	wm.Ydim++
	wm.Zdim = 1
	wm.clearCells(wm.Xdim * wm.Ydim * wm.Zdim)
}

func (wm *WaveMapContents) getSize() int {
	return len(wm.cellArray)
}

func (wm *WaveMapContents) getDimensions() (x, y, z int) {
	return wm.Xdim, wm.Ydim, wm.Zdim
}

func (wm *WaveMapContents) getCell(x, y, z int) *SuperpositionCell {
	index := x + (y * wm.Xdim) + (z * wm.Xdim * wm.Ydim)
	if len(wm.cellArray) < index+1 {
		return nil
	}
	c := wm.cellArray[index]
	return c
}

func (wm *WaveMapContents) getCellByIndex(i int) *SuperpositionCell {
	if len(wm.cellArray) < i+1 {
		return nil
	}
	c := wm.cellArray[i]
	return c
}

func (wm *WaveMapContents) indexToDim(i int) (x, y, z int) {
	z += i / (wm.Xdim * wm.Ydim)
	i = i % (wm.Xdim * wm.Ydim)
	y += i / wm.Xdim
	i = i % wm.Xdim
	x = i
	return
}

func (wm *WaveMapContents) getAllCells() []*SuperpositionCell {
	return wm.cellArray
}

func (wm *WaveMapContents) setCell(x, y, z int, cell *SuperpositionCell) {
	newCell := &SuperpositionCell{weights: make(map[uint16]int)}
	newCell.copyWeights(cell.weights)
	newCell.collapsed = cell.collapsed
	index := x + (y * wm.Xdim) + (z * wm.Xdim * wm.Ydim)
	if len(wm.cellArray) < index+1 {
		panic("index out of bounds")
	}
	wm.cellArray[index] = newCell
}

func (wm *WaveMapContents) clearCells(size int) {
	wm.cellArray = make([]*SuperpositionCell, size)
}

func (wm *WaveMapContents) minEntropy() float64 {
	wm.updateCellEntropies()

	minEntropy := math.MaxFloat64
	for _, cell := range wm.cellArray {
		if cell != nil && !cell.isCollapsed() && cell.entropy < minEntropy {
			minEntropy = cell.entropy
			if minEntropy == 0 {
				break
			}
		}
	}
	// check if the cache would be working
	if wm.minEntropyCacheValid {
		if minEntropy != wm.minEntropyCache {
			panic(fmt.Sprintf("min entropy cache invalid: cache was %f, actual min entropy %f", wm.minEntropyCache, minEntropy))
		// } else {
		// 	fmt.Printf("min entropy cache valid: cache was %f, actual min entropy %f", wm.minEntropyCache, minEntropy)
		}
	}

	// fmt.Println("Min entropy:", minEntropy)
	wm.minEntropyCache = minEntropy
	wm.minEntropyCacheValid = true
	return minEntropy
}

func (wm *WaveMapContents) updateCellEntropies() {
	// Asynchronously make sure the entropy is valid in each interesting cell
	// (not nil or collapsed)

	// Which cells can we fan out?
	fanOuts := make([]*SuperpositionCell, 0, 50)
	for _, cell := range wm.cellArray {
		if cell == nil || cell.isCollapsed() || cell.entropyValid {
			continue
		}
		fanOuts = append(fanOuts, cell)
	}

	if len(fanOuts) > 10 {
		var wg sync.WaitGroup
		wg.Add(len(fanOuts))
		for _, cell := range fanOuts {
			go wm.asyncCalculateEntropy(cell, &wg)
		}
		// fmt.Println("Waiting for entropies:", len(fanOuts))
		wg.Wait()
	} else {
		for _, cell := range fanOuts {
			wm.calculateEntropy(cell)
		}
	}
}

func (wm *WaveMapContents) calculateEntropy(cell *SuperpositionCell) float64 {
	if cell.entropyValid {
		return cell.entropy
	}

	var sum_of_weights int
	var sum_of_wlogw, entropy float64
	for _, tile := range cell.possibleTiles() {
		wInt := wm.TypicalCell.weights[tile]
		sum_of_weights += wInt
		sum_of_wlogw += cachedNLog2N(wInt)
	}
	if sum_of_weights == 0 {
		entropy = 0
	} else {
		intermediate1 := cachedLog2(sum_of_weights)
		intermediate2 := sum_of_wlogw / float64(sum_of_weights)
		entropy = intermediate1 - intermediate2
		if math.Abs(entropy) < 0.00001 {
			entropy = 0
		}
	}
	cell.entropy = entropy
	cell.entropyValid = true

	// fmt.Println("Cell entropy:", entropy)
	return entropy
}

func (wm *WaveMapContents) asyncCalculateEntropy(cell *SuperpositionCell, wg *sync.WaitGroup) {
	defer wg.Done()

	var sum_of_weights int
	var sum_of_wlogw, entropy float64
	for _, tile := range cell.possibleTiles() {
		wInt := wm.TypicalCell.weights[tile]
		sum_of_weights += wInt
		sum_of_wlogw += cachedNLog2N(wInt)
	}
	if sum_of_weights == 0 {
		entropy = 0
	} else {
		intermediate1 := cachedLog2(sum_of_weights)
		intermediate2 := sum_of_wlogw / float64(sum_of_weights)
		entropy = intermediate1 - intermediate2
		if math.Abs(entropy) < 0.00001 {
			entropy = 0
		}
	}
	cell.entropy = entropy
	cell.entropyValid = true

	// fmt.Println("Cell entropy:", entropy)
	return
}

func cachedLog2(n int) float64 {
	log2CacheLock.RLock()
	defer log2CacheLock.RUnlock()
	if len(log2cache) <= n {
		log2CacheLock.RUnlock()
		extendCachedLog2(n)
		log2CacheLock.RLock()
	}
	result := log2cache[n]
	return result
}

func cachedNLog2N(n int) float64 {
	nlog2nCacheLock.RLock()
	defer nlog2nCacheLock.RUnlock()
	if len(nlog2ncache) <= n {
		nlog2nCacheLock.RUnlock()
		extendCachedNLog2N(n)
		nlog2nCacheLock.RLock()
	}
	result := nlog2ncache[n]
	return result
}

func extendCachedLog2(n int) {
	log2CacheLock.Lock()
	defer log2CacheLock.Unlock()
	if len(log2cache) <= n {
		for i := len(log2cache); i < n+1; i++ {
			log2cache = append(log2cache, math.Log2(float64(i)))
		}
	}
	return
}

func extendCachedNLog2N(n int) {
	nlog2nCacheLock.Lock()
	defer nlog2nCacheLock.Unlock()
	if len(nlog2ncache) <= n {
		for i := len(nlog2ncache); i < n+1; i++ {
			nlog2ncache = append(nlog2ncache, float64(i)*math.Log2(float64(i)))
		}
	}
	return
}

// A weight value of zero will mean that tile string is not possible.
type SuperpositionCell struct {
	weights      map[uint16]int
	numPossible  int
	nonzeroTiles []uint16
	entropy      float64
	entropyValid bool
	collapsed    bool
}

// numPossibleTiles returns the number of possible tiles (non-zero weights) in the
// SuperpositionCell.
func (c *SuperpositionCell) numPossibleTiles() int {
	return c.numPossible
}

func (c *SuperpositionCell) calcPossibleTiles() {
	count := 0
	c.nonzeroTiles = make([]uint16, 0, 100)
	for tile, weight := range c.weights {
		if weight > 0 {
			count++
			c.nonzeroTiles = append(c.nonzeroTiles, tile)
		}
	}
	c.numPossible = count
	// make result deterministic by sorting it
	sort.Slice(c.nonzeroTiles, func(i, j int) bool {
		return c.nonzeroTiles[i] < c.nonzeroTiles[j]
	})
}

func (c *SuperpositionCell) copyWeights(src map[uint16]int) {
	c.entropyValid = false
	c.weights = make(map[uint16]int)
	for k, v := range src {
		c.weights[k] = v
	}
	c.calcPossibleTiles()
}

func (c *SuperpositionCell) changeWeight(tile uint16, weight int) {
	c.entropyValid = false
	if weight == 0 {
		c.zeroWeight(tile)
		return
	}
	oldWeight := c.weights[tile]
	c.weights[tile] = weight
	if oldWeight == 0 {
		c.calcPossibleTiles()
		return
	}
}

func (c *SuperpositionCell) decrementWeight(tile uint16) bool {
	c.entropyValid = false
	var causedRemoval bool // true if a tile was removed
	if c.weights[tile] <= 0 {
		return false
	}
	c.weights[tile]--
	if c.weights[tile] == 0 {
		c.numPossible--
		tmpTiles := make([]uint16, 0, len(c.nonzeroTiles))
		for _, t := range c.nonzeroTiles {
			if t != tile {
				tmpTiles = append(tmpTiles, t)
			}
		}
		c.nonzeroTiles = tmpTiles
		delete(c.weights, tile)
		causedRemoval = true
	}
	return causedRemoval
}

func (c *SuperpositionCell) zeroWeight(tile uint16) {
	c.entropyValid = false
	if c.weights[tile] > 0 {
		c.numPossible--
		tmpTiles := make([]uint16, 0, len(c.nonzeroTiles))
		for _, t := range c.nonzeroTiles {
			if t != tile {
				tmpTiles = append(tmpTiles, t)
			}
		}
		c.nonzeroTiles = tmpTiles
	}
	delete(c.weights, tile)
}

// possibleTiles returns a list of possible tiles (non-zero weights) of the SuperpositionCell.
//
// It returns a slice of strings representing the possible tiles.
func (c *SuperpositionCell) possibleTiles() []uint16 {
	return c.nonzeroTiles
}

func (c *SuperpositionCell) only() uint16 {
	if c.numPossibleTiles() == 1 {
		return c.possibleTiles()[0]
	}
	panic("Cell has more than one possible tile:" + fmt.Sprint(c.weights))
}

func (c *SuperpositionCell) collapse(tile uint16) {
	c.entropyValid = false
	for key := range c.weights {
		if key != tile {
			c.zeroWeight(key)
		}
	}
	c.numPossible = 1
	c.collapsed = true
}

func (c *SuperpositionCell) isCollapsed() bool {
	return c.collapsed
}

type DisplayCfg struct {
	Output            *termenv.Output
	FramesPerCollapse uint
	TimePerFrame      time.Duration
	Styled            bool
}

type RetryCfg struct {
	InitialSeed       int64 // Initial random seed
	MaxSeed           int64 // Maximum random seed
	MaxRetriesPerSeed int   // Maximum retries per seed
	SeekOriginal      bool  // Increment seed until output matches original map
	SeekValid         bool  // Increment seed until output is valid
}

func RSTM(header world.MapHeader, contents world.MapContents,
	getInitialWeights func(cells map[world.CellKey]world.Cell) map[uint16]int,
	getInitialWaveMap func(header world.MapHeader, contents world.MapContents, initialWeights map[uint16]int, collapseNulls bool) *WaveMapContents,
	collapseNulls bool,
	getRuleSet func(cells map[world.CellKey]world.Cell) RuleMap,
	setWaveMapEdges func(waveMap *WaveMapContents, rules RuleMap),
	collapseTheCell func(waveMap *WaveMapContents, cellKey world.CellKey, rng *rand.Rand),
	decreaseTheWeights func(waveMap *WaveMapContents, collapsedCell world.CellKey) error,
	retryCfg RetryCfg,
	displayCfg DisplayCfg,
	rng *rand.Rand,
) string {
	// gather the tiles based on the source map; each weight will be 1 in this case.
	initialWeights := getInitialWeights(contents.Cells)

	// initialize the wave map, with all identical cells based on those weights.
	waveMap := getInitialWaveMap(header, contents, initialWeights, collapseNulls)

	// Gather the rule set from the source map.
	ruleSet := getRuleSet(contents.Cells)

	setWaveMapEdges(waveMap, ruleSet)

	// Now collapse the map.
	collapseMap(waveMap, ruleSet, collapseTheCell, ApplyRules, decreaseTheWeights, retryCfg,
		displayCfg, header, rng)

	// Convert the map to a string to return.
	return world.MapToString(header, waveMapToContents(*waveMap, false))
}

// GatherTiles returns a map of tiles in the given map of cells.
//
// It returns a map of tiles and their weights, but in this case each weight is simply 1.
func GatherTiles(cells map[world.CellKey]world.Cell) map[uint16]int {
	populations := make(map[uint16]int)
	for _, cell := range cells {
		populations[world.StringToUint16(cell.Graphic)] = 1
	}
	return populations
}

// GatherTilesIgnoreNull returns a map of tiles in the given map of cells.
//
// It returns a map of tiles and their weights, but in this case each weight is simply 1.
// Null tiles ('  ') are ignored.
func GatherTilesIgnoreNull(cells map[world.CellKey]world.Cell) map[uint16]int {
	populations := make(map[uint16]int)
	for _, cell := range cells {
		populations[world.StringToUint16(cell.Graphic)] = 1
	}
	delete(populations, world.StringToUint16("  "))
	return populations
}

// GatherTileWeights returns a map of tiles in the given map of cells.
//
// It returns a map of tiles and their weights (number of times seen).
// Null tiles ('  ') are ignored.
func GatherTileWeights(cells map[world.CellKey]world.Cell) map[uint16]int {
	populations := make(map[uint16]int)
	for _, cell := range cells {
		populations[world.StringToUint16(cell.Graphic)]++
	}
	delete(populations, world.StringToUint16("  "))
	return populations
}

// CreateWaveMap initializes the wave map with all identical cells based on the provided weights.
//
// It uses the contents of the source map to dictate the dimensions of the wave map.
func CreateWaveMap(header world.MapHeader, contents world.MapContents, initialWeights map[uint16]int, collapseNulls bool) *WaveMapContents {
	// initialize the wave map, with all identical cells based on those weights.
	waveMap := newWaveMap()
	waveMap.XOffset = header.XOffset
	waveMap.YOffset = header.YOffset
	waveMap.ZOffset = header.ZCoord
	waveMap.setDimensions(header, contents)
	for key := range contents.Cells {
		waveMap.setCell(key.X-header.XOffset, key.Y-header.YOffset, key.Z-header.ZCoord,
			&SuperpositionCell{weights: make(map[uint16]int)})
		if collapseNulls && contents.Cells[key].Graphic == "  " {
			waveMap.getCell(key.X-header.XOffset, key.Y-header.YOffset, key.Z-header.ZCoord).changeWeight(world.StringToUint16("  "), 1)
			waveMap.getCell(key.X-header.XOffset, key.Y-header.YOffset, key.Z-header.ZCoord).collapse(world.StringToUint16("  "))
			continue
		}
		waveMap.getCell(key.X-header.XOffset, key.Y-header.YOffset, key.Z-header.ZCoord).copyWeights(initialWeights)
	}
	waveMap.TypicalCell = &SuperpositionCell{}
	waveMap.TypicalCell.copyWeights(initialWeights)
	return waveMap
}

// CreateWaveMapCollapseNulls initializes the wave map with all identical cells based on the provided weights.
//
// It uses the contents of the source map to dictate the dimensions of the wave map. Where the
// source cell is an null tile ('  '), it is treated as already collapsed.
func CreateWaveMapCollapseNulls(header world.MapHeader, contents world.MapContents, initialWeights map[uint16]int) *WaveMapContents {
	// initialize the wave map, with all identical cells based on those weights.
	waveMap := newWaveMap()
	waveMap.XOffset = header.XOffset
	waveMap.YOffset = header.YOffset
	waveMap.ZOffset = header.ZCoord
	waveMap.setDimensions(header, contents)
	for key := range contents.Cells {
		waveMap.setCell(key.X-header.XOffset, key.Y-header.YOffset, key.Z-header.ZCoord,
			&SuperpositionCell{weights: make(map[uint16]int)})
		if contents.Cells[key].Graphic == "  " {
			waveMap.getCell(key.X-header.XOffset, key.Y-header.YOffset, key.Z-header.ZCoord).changeWeight(world.StringToUint16("  "), 1)
			waveMap.getCell(key.X-header.XOffset, key.Y-header.YOffset, key.Z-header.ZCoord).collapse(world.StringToUint16("  "))
			continue
		}
		waveMap.getCell(key.X-header.XOffset, key.Y-header.YOffset, key.Z-header.ZCoord).copyWeights(initialWeights)
	}
	waveMap.TypicalCell = &SuperpositionCell{}
	waveMap.TypicalCell.copyWeights(initialWeights)
	return waveMap
}

func DeriveRuleSet(cells map[world.CellKey]world.Cell) RuleMap {
	return DeriveRuleSetExtended(cells, 1, false)
}

func DeriveRuleSetIgnoreNull(cells map[world.CellKey]world.Cell) RuleMap {
	return DeriveRuleSetExtended(cells, 1, true)
}

func DeriveRuleSetExtended(cells map[world.CellKey]world.Cell, distance int, ignoreNulls bool) RuleMap {
	neighborRules := make(RuleMap)

	for cellKey, cell := range cells {
		if ignoreNulls && cell.Graphic == "  " {
			continue
		}
		for relX := -distance; relX <= distance; relX++ {
			for relY := -distance; relY <= distance; relY++ {
				if relX == 0 && relY == 0 {
					continue
				}
				neighborKey := world.CellKey{X: cellKey.X + relX, Y: cellKey.Y + relY, Z: cellKey.Z}
				neighborGraphic := "  " // default to null
				if neighbor, success := cells[neighborKey]; success {
					neighborGraphic = neighbor.Graphic
				} else if !ignoreNulls {
					continue
				}
				offset := Offset{X: relX, Y: relY}
				if _, success := neighborRules[world.StringToUint16(cell.Graphic)]; !success {
					neighborRules[world.StringToUint16(cell.Graphic)] = make(OffsetMap)
				}
				if _, success := neighborRules[world.StringToUint16(cell.Graphic)][offset]; !success {
					neighborRules[world.StringToUint16(cell.Graphic)][offset] = make(TileMap)
				}
				((neighborRules[world.StringToUint16(cell.Graphic)])[offset])[world.StringToUint16(neighborGraphic)] = true
			}
		}
	}

	return neighborRules
}

func edgeCandidates(rules RuleMap) []uint16 {
	result := []uint16{}
	for home, offsets := range rules {
		for offset, tiles := range offsets {
			if offset.X >= -1 && offset.X <= 1 && offset.Y >= -1 && offset.Y <= 1 && tiles[world.StringToUint16("  ")] {
				if !slices.Contains(result, home) {
					result = append(result, home)
				}
			}
		}
	}

	// make result deterministic by sorting it
	sort.SliceStable(result, func(i, j int) bool {
		return result[i] < result[j]
	})

	return result
}

func SetEdges(waveMap *WaveMapContents, rules RuleMap) {
	edgePossibleTiles := edgeCandidates(rules)

	xSize, ySize, zSize := waveMap.getDimensions()
	for x := 0; x < xSize; x++ {
		for y := 0; y < ySize; y++ {
			for z := 0; z < zSize; z++ {
				cell := waveMap.getCell(x, y, z)
				if cell == nil || cell.isCollapsed() {
					continue // Already collapsed
				}

				if hasMissingNeighbor(*waveMap, world.CellKey{X: x, Y: y, Z: z}) {
					// Neighbor is missing or null tile, remove non-edge tiles
					waveMap.setCell(x, y, z, &SuperpositionCell{weights: map[uint16]int{}})

					for _, tile := range edgePossibleTiles {
						waveMap.getCell(x, y, z).changeWeight(tile, cell.weights[tile])
					}
				}
			}
		}
	}
}

func SetEdgesExtended(waveMap *WaveMapContents, rules RuleMap, distance int) {
	xSize, ySize, zSize := waveMap.getDimensions()
	for x := 0; x < xSize; x++ {
		for y := 0; y < ySize; y++ {
			for z := 0; z < zSize; z++ {
				cell := waveMap.getCell(x, y, z)
				if cell == nil || cell.isCollapsed() {
					continue // Already collapsed
				}

				// Loop through the neighbors to limit their possible tiles.
				for relX := -distance; relX <= distance; relX++ {
					for relY := -distance; relY <= distance; relY++ {
						if relX == 0 && relY == 0 {
							continue // self, not neighbor
						}
						// Check if neighbor exists
						var neighborMissing bool
						if x+relX < 0 || y+relY < 0 || x+relX > waveMap.Xdim-1 || y+relY > waveMap.Ydim-1 {
							neighborMissing = true // neighbor is out of bounds
						} else {
							neighbor := waveMap.getCell(x+relX, y+relY, z)
							if neighbor == nil || (neighbor.numPossibleTiles() == 1 && neighbor.only() == world.StringToUint16("  ")) {
								neighborMissing = true
							}
						}
						if neighborMissing {
							// Neighbor is missing or null, remove tiles that do not allow null with that offset
							offset := Offset{X: relX, Y: relY}
							for _, tile := range cell.possibleTiles() {
								if !rules[tile][offset][world.StringToUint16("  ")] {
									cell.zeroWeight(tile)
								}
							}
						}
					}
				}
			}
		}
	}
}

func hasMissingNeighbor(waveMap WaveMapContents, cellKey world.CellKey) bool {
	if cellKey.X < 1 || cellKey.Y < 1 || cellKey.X >= waveMap.Xdim-1 || cellKey.Y >= waveMap.Ydim-1 {
		return true
	}
	for relX := -1; relX <= 1; relX++ {
		for relY := -1; relY <= 1; relY++ {
			if relX == 0 && relY == 0 {
				continue
			}
			neighbor := waveMap.getCell(cellKey.X+relX, cellKey.Y+relY, cellKey.Z)
			if neighbor == nil || (neighbor.numPossibleTiles() == 1 && neighbor.only() == world.StringToUint16("  ")) {
				return true
			}
		}
	}
	return false
}

func collapseMap(waveMap *WaveMapContents, ruleSet RuleMap,
	collapseTheCell func(waveMap *WaveMapContents, cellKey world.CellKey, rng *rand.Rand),
	applyTheRules func(waveMap *WaveMapContents, ruleSet RuleMap, cellKey world.CellKey) error,
	decreaseTheWeights func(waveMap *WaveMapContents, collapsedCell world.CellKey) error,
	retryCfg RetryCfg,
	displayCfg DisplayCfg,
	header world.MapHeader,
	rng *rand.Rand,
) {
	var err error

	// Back up the wave map to restore in case of impossible state
	var bakWaveMap *WaveMapContents
	if retryCfg.MaxRetriesPerSeed > 0 {
		bakWaveMap = newWaveMap()
		copyWaveMap(bakWaveMap, waveMap)
	}

	totalCells := waveMap.getNumCells()
	totalCollapsed := countCollapsed(waveMap)

	retries := 0

	// Loop until all cells are collapsed
	for totalCollapsed < totalCells {

		// Gather the cells with minimum entropy and choose one.
		minEntropyCellList, minEntropy := minEntropyCellsIndex(waveMap)
		randIndex := rng.Intn(len(minEntropyCellList))
		x, y, z := waveMap.indexToDim(minEntropyCellList[randIndex])
		cellToCollapse := world.CellKey{X: x, Y: y, Z: z}

		// Collapse the cell
		collapseTheCell(waveMap, cellToCollapse, rng)
		if len(minEntropyCellList) == 1 {
			// we just collapsed the last one with min entropy
			waveMap.minEntropyCacheValid = false
		}
		totalCollapsed++

		// Apply the adjacency rules
		err = applyTheRules(waveMap, ruleSet, cellToCollapse)
		if err != nil {
			retries++
			if retries > retryCfg.MaxRetriesPerSeed {
				return
			}

			// If we've hit an impossible state, restore from backup
			copyWaveMap(waveMap, bakWaveMap)
			totalCollapsed = countCollapsed(waveMap)
			continue
		}

		// decrease the weights based on the collapsed cell's tile.
		err = decreaseTheWeights(waveMap, cellToCollapse)
		if err != nil {
			retries++
			if retries > retryCfg.MaxRetriesPerSeed {
				return
			}

			// If we've hit an impossible map, restore from backup
			copyWaveMap(waveMap, bakWaveMap)
			totalCollapsed = countCollapsed(waveMap)
			continue
		}

		// Display the map, in the upper-left corner of the terminal. For cells
		//   that haven't been collapsed, we choose a possible tile at random, so
		//   displaying multiple times with a chosen interval can make for a nice
		//   animation.
		if displayCfg.FramesPerCollapse > 0 && minEntropy != 0 {
			for i := uint(0); i < displayCfg.FramesPerCollapse; i++ {
				textMap := world.MapToString(header, waveMapToContents(*waveMap, displayCfg.Styled))
				displayCfg.Output.MoveCursor(0, 0)
				displayCfg.Output.WriteString(textMap)
				time.Sleep(displayCfg.TimePerFrame)
			}
		}
	}
}

func collapseMapExtended(waveMap *WaveMapContents, ruleSet RuleMap,
	collapseTheCell func(waveMap *WaveMapContents, cellKey world.CellKey, rng *rand.Rand),
	applyTheRules func(waveMap *WaveMapContents, ruleSet RuleMap, cellKey world.CellKey, distance int) error,
	decreaseTheWeights func(waveMap *WaveMapContents, collapsedCell world.CellKey) error,
	distance int,
	retryCfg RetryCfg,
	displayCfg DisplayCfg,
	header world.MapHeader,
	rng *rand.Rand,
) {
	var err error

	// Back up the wave map to restore in case of impossible state
	var bakWaveMap *WaveMapContents
	if retryCfg.MaxRetriesPerSeed > 0 {
		bakWaveMap = newWaveMap()
		copyWaveMap(bakWaveMap, waveMap)
	}

	totalCells := waveMap.getNumCells()
	totalCollapsed := countCollapsed(waveMap)

	retries := 0

	// Loop until all cells are collapsed
	for totalCollapsed < totalCells {

		// Gather the cells with minimum entropy and choose one.
		minEntropyCellList, minEntropy := minEntropyCellsIndex(waveMap)
		randIndex := rng.Intn(len(minEntropyCellList))
		x, y, z := waveMap.indexToDim(minEntropyCellList[randIndex])
		cellToCollapse := world.CellKey{X: x, Y: y, Z: z}

		// Collapse the cell
		// fmt.Println("Collapsing", cellToCollapse)
		collapseTheCell(waveMap, cellToCollapse, rng)
		if len(minEntropyCellList) == 1 {
			// we just collapsed the last one with min entropy
			waveMap.minEntropyCacheValid = false
		}
		totalCollapsed++

		// Apply the adjacency rules
		err = applyTheRules(waveMap, ruleSet, cellToCollapse, distance)
		if err != nil {
			retries++
			if retries > retryCfg.MaxRetriesPerSeed {
				return
			}

			// If we've hit an impossible state, restore from backup
			copyWaveMap(waveMap, bakWaveMap)
			totalCollapsed = countCollapsed(waveMap)
			continue
		}

		// decrease the weights based on the collapsed cell's tile.
		err = decreaseTheWeights(waveMap, cellToCollapse)
		if err != nil {
			retries++
			if retries > retryCfg.MaxRetriesPerSeed {
				return
			}

			// If we've hit an impossible map, restore from backup
			copyWaveMap(waveMap, bakWaveMap)
			totalCollapsed = countCollapsed(waveMap)
			continue
		}

		// Display the map, in the upper-left corner of the terminal. For cells
		//   that haven't been collapsed, we choose a possible tile at random, so
		//   displaying multiple times with a chosen interval can make for a nice
		//   animation.
		if displayCfg.FramesPerCollapse > 0 && minEntropy != 0 {
			for i := uint(0); i < displayCfg.FramesPerCollapse; i++ {
				textMap := world.MapToString(header, waveMapToContents(*waveMap, displayCfg.Styled))
				displayCfg.Output.MoveCursor(0, 0)
				displayCfg.Output.WriteString(textMap)
				// fmt.Print(textMap)
				time.Sleep(displayCfg.TimePerFrame)
			}
		}
	}
}

func copyWaveMap(dest *WaveMapContents, src *WaveMapContents) {
	dest.clearCells(len(src.cellArray))
	dest.XOffset = src.XOffset
	dest.YOffset = src.YOffset
	dest.ZOffset = src.ZOffset
	dest.Xdim, dest.Ydim, dest.Zdim = src.getDimensions()
	for i := 0; i < len(src.cellArray); i++ {
		if src.cellArray[i] == nil {
			continue
		}
		dest.cellArray[i] = &SuperpositionCell{}
		dest.cellArray[i].copyWeights(src.cellArray[i].weights)
		dest.cellArray[i].collapsed = src.cellArray[i].collapsed
	}

	dest.TypicalCell = &SuperpositionCell{}
	dest.TypicalCell.copyWeights(src.TypicalCell.weights)
}

func countCollapsed(waveMap *WaveMapContents) int {
	result := 0
	xSize, ySize, zSize := waveMap.getDimensions()
	for x := 0; x < xSize; x++ {
		for y := 0; y < ySize; y++ {
			for z := 0; z < zSize; z++ {
				cell := waveMap.getCell(x, y, z)
				if cell != nil && waveMap.getCell(x, y, z).isCollapsed() {
					result++
				}
			}
		}
	}

	return result
}

// waveMapToContents converts a wave map to a world.MapContents struct.
//
// It applies styles to the cells based on the cell entropy, and choosing a random possible tile
//
//	for those cells which have not yet been collapsed.
func waveMapToContents(waveMap WaveMapContents, styled bool) world.MapContents {
	rng := rand.New(rand.NewSource(time.Now().UnixNano())) // just for visuals
	contents := world.MapContents{Cells: make(map[world.CellKey]world.Cell, waveMap.getNumCells())}
	var currentMinEntropy, currentTypicalEntropy float64
	entropyMap := make(map[float64]int)
	if styled {
		currentMinEntropy = waveMap.minEntropy()
		currentTypicalEntropy = waveMap.calculateEntropy(waveMap.TypicalCell)
	}
	xSize, ySize, zSize := waveMap.getDimensions()
	for x := 0; x < xSize; x++ {
		for y := 0; y < ySize; y++ {
			for z := 0; z < zSize; z++ {
				cell := waveMap.getCell(x, y, z)
				if cell == nil {
					continue
				}
				key := world.CellKey{X: x + waveMap.XOffset, Y: y + waveMap.YOffset, Z: z + waveMap.ZOffset}
				style := lipgloss.NewStyle()
				if cell.numPossibleTiles() < 1 {
					if styled {
						style = style.Background(lipgloss.Color("#F00000"))
						contents.Cells[key] = world.Cell{Graphic: style.Render("💀")}
					} else {
						contents.Cells[key] = world.Cell{Graphic: "XX"}
					}
					continue
				}
				if !cell.isCollapsed() { // cell not yet collapsed
					if styled {
						cellEntropy := waveMap.calculateEntropy(cell)
						var normalizedEntropy float64
						if currentTypicalEntropy-currentMinEntropy != 0 {
							normalizedEntropy = (cellEntropy - currentMinEntropy) / (currentTypicalEntropy - currentMinEntropy)
						}
						if normalizedEntropy > 1 {
							normalizedEntropy = 1
						}
						entropyMap[float64(int(normalizedEntropy*1000))/1000]++
						reddish := "00"
						greenish := fmt.Sprintf("%02X", int((1.0-normalizedEntropy)*0xd0))
						blueish := fmt.Sprintf("%02X", int(normalizedEntropy*0xff))
						style = style.Bold(false).Background(lipgloss.Color("#" + reddish + greenish + blueish))

						// choose a possible tile at random
						whichTile := weightedRandomTile(cell.weights, rng)
						contents.Cells[key] = world.Cell{Graphic: style.Render(world.Uint16ToString(whichTile))}
					} else {
						contents.Cells[key] = world.Cell{Graphic: "?!"}
					}
				} else { // collapsed cell
					if styled {
						style = style.Bold(true)
						contents.Cells[key] = world.Cell{Graphic: style.Render(world.Uint16ToString(cell.only()))}
					} else {
						contents.Cells[key] = world.Cell{Graphic: world.Uint16ToString(cell.only())}
					}
				}
			}
		}
	}

	return contents
}

// CollapseCell collapses a cell in the wave map, choosing a possible tile at random.
func CollapseCell(waveMap *WaveMapContents, cellKey world.CellKey, rng *rand.Rand) {
	cell := waveMap.getCell(cellKey.X, cellKey.Y, cellKey.Z)
	possibleTiles := cell.possibleTiles()
	// fmt.Println("Possible tiles for cell", cellKey, ":", possibleTiles)

	chosenIndex := rng.Intn(len(possibleTiles))
	chosenTile := possibleTiles[chosenIndex]
	cell.collapse(chosenTile)
	// waveMap.minEntropyValid = false
}

// CollapseCellWeighted collapses a cell in the wave map, choosing a possible tile at random but
// considering the tile weights.
func CollapseCellWeighted(waveMap *WaveMapContents, cellKey world.CellKey, rng *rand.Rand) {
	cell := waveMap.getCell(cellKey.X, cellKey.Y, cellKey.Z)
	chosenTile := weightedRandomTile(cell.weights, rng)
	cell.collapse(chosenTile)
	// waveMap.minEntropyValid = false
}

func weightedRandomTile(weights map[uint16]int, rng *rand.Rand) uint16 {
	tiles := make([]uint16, 0, len(weights))
	totalWeight := 0
	for key, weight := range weights {
		totalWeight += weight
		tiles = append(tiles, key)
	}

	// Sort for deterministic behavior
	sort.Slice(tiles, func(i, j int) bool {
		return tiles[i] > tiles[j]
	})

	guess := rng.Intn(totalWeight)
	for _, tile := range tiles {
		guess -= weights[tile]
		if guess < 0 {
			return tile
		}
	}

	panic("Weights changed during random pick.")
}

func minEntropyCellsIndex(waveMap *WaveMapContents) ([]int, float64) {

	// In theory, if the minEntropyCache is valid, none of the cell entropies have changed.
	var minEntropy float64
	if waveMap.minEntropyCacheValid {
		minEntropy = waveMap.minEntropyCache
		// fmt.Printf("Using cached minEntropy: %f\n", minEntropy)
	} else {
		minEntropy = waveMap.minEntropy() // this will make cell entropies valid
		// fmt.Printf("Calculated minEntropy: %f\n", minEntropy)
	}

	cells := make([]int, 0, 1000)

	// We can assume all entropy caches are valid at this point, since we already called
	// minEntropy() above.

	for i, cell := range waveMap.getAllCells() {
		if cell == nil || cell.isCollapsed() {
			continue
		}
		if cell.entropy == minEntropy {
			cells = append(cells, i)
		}
	}

	// fmt.Println("Cells with min entropy", minEntropy, ":", cells)
	return cells, minEntropy
}

// ApplyRules applies the given rule set to neighbors of a specific cell in the wave map.
func ApplyRules(waveMap *WaveMapContents, ruleSet RuleMap, cellKey world.CellKey) error {
	// Loop through the rule set to gather possible neighbor tiles.
	cell := waveMap.getCell(cellKey.X, cellKey.Y, cellKey.Z)
	cellTile := cell.only()

	// Loop through the neighbors to limit their possible tiles.
	for relX := -1; relX <= 1; relX++ {
		for relY := -1; relY <= 1; relY++ {
			if relX == 0 && relY == 0 {
				continue // self, not neighbor
			}
			if cellKey.X+relX < 1 || cellKey.Y+relY < 1 || cellKey.X+relX >= waveMap.Xdim-1 || cellKey.Y+relY >= waveMap.Ydim-1 {
				continue // out of bounds
			}
			// Does such a neighbor exist?
			var neighborTile uint16
			neighbor := waveMap.getCell(cellKey.X+relX, cellKey.Y+relY, cellKey.Z)
			offset := Offset{X: relX, Y: relY}
			if neighbor != nil && !neighbor.isCollapsed() {
				// Loop through the neighbor's possible tiles.
				for _, neighborTile = range neighbor.possibleTiles() {
					// Is this tile not one of the possible neighbors?
					if !ruleSet[cellTile][offset][neighborTile] {
						neighbor.zeroWeight(neighborTile) // Zero out its weight.
						waveMap.minEntropyCacheValid = false
					}
				}
				// If the neighbor now has no possible tiles, return an error.
				if neighbor.numPossibleTiles() == 0 {
					neighborKey := world.CellKey{X: cellKey.X + relX, Y: cellKey.Y + relY, Z: cellKey.Z}
					return errors.New(fmt.Sprint("After tile", world.Uint16ToString(cellTile), "(", cellTile,
						") denies neighbor ", world.Uint16ToString(neighborTile), "(", neighborTile, "), cell",
						neighborKey, "has no possible tiles", neighbor))
				}
			}
		}
	}
	return nil
}

func ApplyRulesDirectional(waveMap *WaveMapContents, ruleSet RuleMap, cellKey world.CellKey) error {
	return ApplyRulesExtended(waveMap, ruleSet, cellKey, 1)
}

func ApplyRulesExtended(waveMap *WaveMapContents, ruleSet RuleMap, cellKey world.CellKey, distance int) error {
	// Loop through the rule set to gather possible neighbor tiles.
	cell := waveMap.getCell(cellKey.X, cellKey.Y, cellKey.Z)
	cellTile := cell.only()
	offsets := ruleSet[cellTile]

	// Loop through the neighbors to limit their possible tiles.
	for relX := -distance; relX <= distance; relX++ {
		for relY := -distance; relY <= distance; relY++ {
			if relX == 0 && relY == 0 {
				continue // self, not neighbor
			}
			if cellKey.X+relX < 0 || cellKey.Y+relY < 0 || cellKey.X+relX > waveMap.Xdim-1 || cellKey.Y+relY > waveMap.Ydim-1 {
				continue // out of bounds
			}
			// Does such a neighbor exist?
			var neighborTile uint16
			offset := Offset{X: relX, Y: relY}
			neighbor := waveMap.getCell(cellKey.X+relX, cellKey.Y+relY, cellKey.Z)
			if neighbor != nil && !neighbor.isCollapsed() {
				// Loop through the neighbor's possible tiles
				allowedTiles := offsets[offset]
				for _, neighborTile = range neighbor.possibleTiles() {
					if !allowedTiles[neighborTile] {
						neighbor.zeroWeight(neighborTile) // Zero out its weight.
						waveMap.minEntropyCacheValid = false
					}
				}
				if neighbor.numPossibleTiles() == 0 {
					neighborKey := world.CellKey{X: cellKey.X + relX, Y: cellKey.Y + relY, Z: cellKey.Z}
					return errors.New(fmt.Sprint("After tile", world.Uint16ToString(cellTile), "(", cellTile,
						") denies neighbor ", world.Uint16ToString(neighborTile), "(", neighborTile, "), cell",
						neighborKey, "has no possible tiles", neighbor))
				}
			}
		}
	}
	return nil
}

func DecreaseWeights(waveMap *WaveMapContents, collapsedCell world.CellKey) error {
	waveMap.minEntropyCacheValid = false
	tile := waveMap.getCell(collapsedCell.X, collapsedCell.Y, collapsedCell.Z).only()
	if waveMap.TypicalCell.decrementWeight(tile) {
		// Decrementing caused tile's removal, remove from all active cells
		size := waveMap.getSize()
		for i := 0; i < size; i++ {
			cell := waveMap.getCellByIndex(i)
			if cell == nil || cell.isCollapsed() {
				// the cell itself should already be collapsed, so this skips that too.
				continue
			}
			cell.zeroWeight(tile) // also invalidates entropy cache
			if cell.numPossibleTiles() < 1 {
				return errors.New(fmt.Sprint("After decreasing weight of tile ", world.Uint16ToString(tile), "(", tile, ") in cell ",
					i, "has no possible tiles", cell))
			}
		}
		return nil
	}
	// Invalidate the entropy caches of any affected cells (i.e. cells that
	// haven't had that tile zeroed out).
	size := waveMap.getSize()
	for i := 0; i < size; i++ {
		cell := waveMap.getCellByIndex(i)
		if cell == nil || cell.isCollapsed() || cell.weights[tile] == 0 {
			continue
		}
		cell.entropyValid = false
	}

	return nil
}

func DecreaseWeightsBadCache(waveMap *WaveMapContents, collapsedCell world.CellKey) error {
	waveMap.minEntropyCacheValid = false
	tile := waveMap.getCell(collapsedCell.X, collapsedCell.Y, collapsedCell.Z).only()
	if waveMap.TypicalCell.decrementWeight(tile) {
		// Decrementing caused tile's removal, remove from all active cells
		size := waveMap.getSize()
		for i := 0; i < size; i++ {
			cell := waveMap.getCellByIndex(i)
			if cell == nil || cell.isCollapsed() {
				// the cell itself should already be collapsed, so this skips that too.
				continue
			}
			cell.zeroWeight(tile) // also invalidates entropy cache
			if cell.numPossibleTiles() < 1 {
				return errors.New(fmt.Sprint("After decreasing weight of tile ", world.Uint16ToString(tile), "(", tile, ") in cell ",
					i, "has no possible tiles", cell))
			}
		}
	}
	return nil
}

func XSTM(header world.MapHeader, contents world.MapContents,
	getInitialWeights func(cells map[world.CellKey]world.Cell) map[uint16]int,
	getInitialWaveMap func(header world.MapHeader, contents world.MapContents, initialWeights map[uint16]int, collapseNulls bool) *WaveMapContents,
	collapseNulls bool,
	getRuleSetExtended func(cells map[world.CellKey]world.Cell, distance int, ignoreNulls bool) RuleMap,
	neighborDistance int,
	ignoreNulls bool,
	setWaveMapEdgesExtended func(waveMap *WaveMapContents, rules RuleMap, distance int),
	collapseTheCell func(waveMap *WaveMapContents, cellKey world.CellKey, rng *rand.Rand),
	decreaseTheWeights func(waveMap *WaveMapContents, collapsedCell world.CellKey) error,
	retryCfg RetryCfg,
	displayCfg DisplayCfg,
	origMap string,
) string {
	// gather the tiles based on the source map
	initialWeights := getInitialWeights(contents.Cells)

	// initialize the wave map, with all identical cells based on those weights.
	waveMap := getInitialWaveMap(header, contents, initialWeights, collapseNulls)

	// Gather the rule set from the source map.
	ruleSet := getRuleSetExtended(contents.Cells, neighborDistance, ignoreNulls)

	setWaveMapEdgesExtended(waveMap, ruleSet, neighborDistance)

	// make a copy of the wave map to restore each loop
	originalWaveMap := newWaveMap()
	copyWaveMap(originalWaveMap, waveMap)

	// set up for the main loop
	var finalMap string
	var done bool
	seed := retryCfg.InitialSeed
	// If either "seek" flag is set, keep calling the method until:
	// 1) if seek-original, the output is the same as the original map,
	// 2) if seek-valid, the collapse completes with a valid map,
	// 3) if max-seed is set, the maximum seed is reached.
	var lastMinute, currMinute int
	lastMinute = time.Now().Minute()

	for !done {
		done = true
		rng := rand.New(rand.NewSource(seed))
		currMinute = time.Now().Minute()
		if displayCfg.FramesPerCollapse > 0 || currMinute != lastMinute {
			// fmt.Println(finalMap)
			displayCfg.Output.WriteString(fmt.Sprint("Processing seed: ", seed, "\n"))
			lastMinute = currMinute
		}

		// Copy original wave map.
		waveMap = newWaveMap()
		copyWaveMap(waveMap, originalWaveMap)
		// Now collapse the map.
		collapseMapExtended(waveMap, ruleSet, collapseTheCell, ApplyRulesExtended, decreaseTheWeights, neighborDistance, retryCfg,
			displayCfg, header, rng)
		finalMap = world.MapToString(header, waveMapToContents(*waveMap, false))

		if retryCfg.SeekValid && strings.Contains(finalMap, "XX") {
			done = false
		}
		if retryCfg.SeekOriginal && finalMap != origMap {
			done = false
		}
		if retryCfg.MaxSeed != 0 && seed >= retryCfg.MaxSeed {
			done = true
		}
		if done {
			displayCfg.Output.WriteString(fmt.Sprint("Seed: ", seed, "\n"))
		}
		seed++
	}

	// Convert the map to a string to return.
	return finalMap
}
